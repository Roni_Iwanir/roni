
Groups={'Left_Wing','Central','Right_Wing'};
for group_num=1:length(Groups)
    original_stimuli=dir(['./',Groups{group_num},'/photoshop/*.jpg']);
    old_path=['./',Groups{group_num},'/photoshop/'];
    new_path=['./All_stimuli_renamed/'];
    for stim=1:length(original_stimuli)
        old_name=original_stimuli(stim).name;
        new_name=[num2str(group_num*100+stim),'_',old_name];
        copyfile([old_path,old_name],[new_path,new_name]);
    end
end
