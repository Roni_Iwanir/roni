function introduction()
%% 'INITIALIZE SCREEN'
% -----------------------------------------------


Screen('Preference', 'VisualDebuglevel', 3); %No PTB intro screen
Screen('Screens')
screennum = max(Screen('Screens'))

pixelSize=32
% [w] = Screen('OpenWindow',screennum,[],[0 0 800 600],pixelSize);% %debugging screensize
[w] = Screen('OpenWindow',screennum,[],[],pixelSize);

%   colors
% - - - - - -
black = BlackIndex(w); % Should equal 0.
white = WhiteIndex(w); % Should equal 255.
Green = [0 255 0];

Screen('FillRect', w, black);
Screen('Flip', w);

%   text
% - - - - - -
theFont='Arial';
Screen('TextSize',w,36);
Screen('TextFont',w,theFont);
Screen('TextColor',w,white);

WaitSecs(1);
HideCursor;


Introduction=dir('./Instructions/Introduction.JPG');
Introduction_name=struct2cell(rmfield(Introduction,{'date','bytes','isdir','datenum'}));
Introduction_image=imread(['./Instructions/' sprintf(Introduction_name{1})]);

% Show Introduction
Screen('PutImage',w,Introduction_image);
Screen(w,'Flip');

KbQueueCreate;
KbQueueStart;

noresp=1;
while noresp,
    [keyIsDown] = KbCheck(-1); % deviceNumber=keyboard
    if keyIsDown && noresp,
        noresp=0;
    end;
end;

KbQueueFlush;
KbQueueStart;
