function recognition_confidence_demo(~,test_comp,mainPath)

% function recognition_confidence(subjectID,test_comp,mainPath,order, sessionNum)

% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% =============== Created based on the previous boost codes ===============
% ==================== by Rotem Botvinik November 2015 ====================
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

% This function runs the recognition session of the cue-approach task.
% Subjects are presented with the stimuli from the previous sessions (but
% only those that were part of the probe comparisons- as defined hard-coded in this function)
% as well as some items that were not included in the training session, in a random order, and
% should answer whether they recognize each stimuli from the previous
% sessions or not.
% In this version, subjects also indicate their level of confidence
% (high/low/uncertain).
% Then, for every item, they are immediately asked whether this item was paired with a beep during training, while
% again indicating their level of confidence.


% Old stimuli should be located in the folder [mainPath'/stim/']
% New stimuli should be located in the folder [mainPath'/stim/recognitionNew/']

% This version of the function fits the boost version with training only 40
% items!


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% % % --------- Exterior files needed for task to run correctly: ----------
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%   'stopGoList_allstim_order%d.txt', order --> Created by the 'sortBdm_Israel' function


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% % % ------------------- Creates the following files: --------------------
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%   'recognition_confidence_results' num2str(sessionNum) '_' timestamp '.txt''
%   --> Which includes the variables: trialNum (in the newOld recognition task), index
%   of the item (ABC order, old and than new), name of item, whether the item
%   is old (1) or not (0), the subject's answer (1- high confidence old; 2-
%   low confidence old; 3- uncertain; 4- low confidence new; 5- high
%   confidence new), onset time, response key, RT, whether the item was paired with a beep (0- no ; 1-
%   yes) and the subject answer on the beep/nobeep question (0-wasn't
%   asked; 1- high confidence yes; 2- low confidence yes; 3- uncertain; 4-
%   low confidence no; 5- high confidence no), onset time, response key,
%   RT.


% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% % ------------------- dummy info for testing purposes -------------------
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
% subjectID = 'test999';
% order = 1;
% test_comp = 4;
% sessionNum = 1;
% mainPath = '/Users/schonberglabimac1/Documents/BMI_BS_40';


tic

rng shuffle
% Screen('Preference', 'SkipSyncTests', 1);

%==========================================================
%% 'INITIALIZE Screen variables to be used in each task'
%==========================================================

Screen('Preference', 'VisualDebuglevel', 3); %No PTB intro screen
screennum = max(Screen('Screens'));

pixelSize = 32;
% [w] = Screen('OpenWindow',screennum,[],[0 0 640 480],pixelSize);% %debugging screensize
[w] = Screen('OpenWindow',screennum,[],[],pixelSize);

%{
[wWidth, wHeight] = Screen('WindowSize', w);
xcenter = wWidth/2;
ycenter = wHeight/2;


sizeFactor = 0.8;
stimW = 576*sizeFactor;
stimH = 432*sizeFactor;
rect = [xcenter-stimW/2 ycenter-stimH/2 xcenter+stimW/2 ycenter+stimH/2];
%}

% Colors settings
black = BlackIndex(w); % Should equal 0.
white = WhiteIndex(w); % Should equal 255.
% green = [0 255 0];

Screen('FillRect', w, black);
Screen('Flip', w);

% set up screen positions for stimuli
[wWidth, wHeight] = Screen('WindowSize', w);
xcenter = wWidth/2;
ycenter = wHeight/2;


% text settings
theFont = 'Arial';
Screen('TextFont',w,theFont);
Screen('TextSize',w, 40);

HideCursor;

% -----------------------------------------------
%% Load Instructions
% -----------------------------------------------

% Load Hebrew instructions image files
Instructions = dir([mainPath '/Instructions/recognition_confidence_demo.JPG' ]);
Instructions_fmri = dir([mainPath '/Instructions/recognition_confidence_demo.JPG' ]);
Instructions_image = imread([mainPath '/Instructions/' Instructions(1).name]);
Instructions_image_fmri = imread([mainPath '/Instructions/' Instructions_fmri(1).name]);

% -----------------------------------------------
%% Load Questions Images
% -----------------------------------------------

% Load Hebrew questions image files
goNoGoQuestion = dir([mainPath '/Instructions/recognition_goNoGo_question.JPG' ]);
familiarityQuestion = dir([mainPath '/Instructions/recognition_familiarity_question.jpg' ]);
goNoGoAnswers = dir([mainPath '/Instructions/recognition_goNoGo_answers.jpg' ]);
familiarityAnswers = dir([mainPath '/Instructions/recognition_familiarity_answers.jpg' ]);

goNoGoQuestion_image = imread([mainPath '/Instructions/' goNoGoQuestion(1).name]);
familiarityQuestion_image = imread([mainPath '/Instructions/' familiarityQuestion(1).name]);
goNoGoAnswers_image=imread([mainPath '/Instructions/' goNoGoAnswers(1).name]);
familiarityAnswers_image=imread([mainPath '/Instructions/' familiarityAnswers(1).name]);

% size_goNoGoQuestion_image = size(goNoGoQuestion_image);
% size_goNoGoQuestion_image = size_goNoGoQuestion_image(1:2);
question1_height = size(familiarityQuestion_image,1);
question1_width = size(familiarityQuestion_image,2);
question2_height = size(goNoGoQuestion_image,1);
question2_width = size(goNoGoQuestion_image,2);

question1_location = [xcenter-question1_width/2 0.3*ycenter-question1_height/2 xcenter+question1_width/2 0.3*ycenter+question1_height/2];
question2_location = [xcenter-question2_width/2 0.3*ycenter-question2_height/2 xcenter+question2_width/2 0.3*ycenter+question2_height/2];

% -----------------------------------------------
%% Load Answers Images
% -----------------------------------------------

% the dimensions of the answers' images are 1288 X 142
answers_width = 1280;
answers_height = 210;
answers_location = [xcenter-answers_width/2 1.75*ycenter-answers_height/2 xcenter+answers_width/2 1.75*ycenter+answers_height/2];

%---------------------------------------------------------------
%% 'Assign response keys'
%---------------------------------------------------------------
KbName('UnifyKeyNames');

if test_comp == 1
%     leftresp = 'b';
%     rightresp = 'y';
    %     badresp = 'x';
else
%     leftresp = 'u';
%     rightresp = 'i';
    %     badresp = 'x';
    key1 = '1'; % high confidence yes / beep
    key2 = '2'; % low confidence yes / beep
    key3 = '3'; % uncertain
    key4 = '4'; % low confidence no / no beep
    key5 = '5'; % high confidence no / no beep
end

%---------------------------------------------------------------
%% 'LOAD image arrays'
%---------------------------------------------------------------

demoStimName = dir([mainPath '/Stim/demo/*.jpg']); % Read demo stimuli

% Read old images to a cell array
imgArraysDemo = cell(1,length(demoStimName));
for i = 1:length(demoStimName)
    imgArraysDemo{i} = imread([mainPath '/Stim/demo/' demoStimName(i).name]);
end;


%---------------------------------------------------------------
%% 'SHUFFLE data about the stimuli - Go \ NoGo
%---------------------------------------------------------------


imgArrays = imgArraysDemo;

%---------------------------------------------------------------
%% 'Display Main Instructions'
%---------------------------------------------------------------

Screen('TextSize',w, 40);

if test_comp == 1
   Screen('PutImage',w,Instructions_image_fmri);    
    
else
   Screen('PutImage',w,Instructions_image);    
end
Screen(w,'Flip');


noresp = 1;
while noresp,
    [keyIsDown] = KbCheck(-1); % deviceNumber=keyboard
    if keyIsDown && noresp,
        noresp = 0;
    end;
end

if test_comp == 1
    CenterText(w,'GET READY! Waiting for trigger', white, 0, 0);
    Screen('Flip',w);
    % escapeKey = KbName('space');
    escapeKey = KbName('t');
    while 1
        [keyIsDown,~,keyCode] = KbCheck(-1);
        if keyIsDown && keyCode(escapeKey)
            break;
        end
    end    
    DisableKeysForKbCheck(KbName('t')); % So trigger is no longer detected  
end; % end if test_comp == 1

WaitSecs(0.001);
% anchor = GetSecs;

Screen('TextSize',w, 60);
Screen('DrawText', w, '+', xcenter, ycenter, white);
Screen(w,'Flip');
WaitSecs(1);

KbQueueCreate;

%---------------------------------------------------------------
%% 'Run Trials'
%---------------------------------------------------------------

% for trial = 1:6 % for debugging
for trial = 1:length(demoStimName)
    
    % isFamiliar part
    
    %-----------------------------------------------------------------
    % display image
    % self-paced; next image will only show after response
    
    Screen('PutImage',w, imgArrays{trial}); % display item
    Screen('TextSize',w, 40);
    Screen('PutImage',w, familiarityQuestion_image,question1_location); % display question
    Screen('PutImage',w, familiarityAnswers_image,answers_location); % display answers
    Screen(w,'Flip');
    
    KbQueueStart;
    %-----------------------------------------------------------------
    % get response
    
    
    noresp = 1;
    while noresp
        % check for response
        [keyIsDown, firstPress] = KbQueueCheck(-1);
        
        if keyIsDown && noresp
            findfirstPress = find(firstPress);
            tmp = KbName(findfirstPress);
            if ischar(tmp)==0 % if 2 keys are hit at once, they become a cell, not a char. we need keyPressed to be a char, so this converts it and takes the first key pressed
                tmp = char(tmp);
            end
            response_isFamiliar = tmp(1);
            if response_isFamiliar==key1||response_isFamiliar==key2||response_isFamiliar==key3||response_isFamiliar==key4||response_isFamiliar==key5 % A valid response is only 1,2,3,4 or 5
            noresp = 0;
            end
            
        end % end if keyIsDown && noresp
        
    end % end while noresp

    
    %-----------------------------------------------------------------
    % redraw text output with the appropriate colorchanges to highlight
    % response
    offset=(str2double(response_isFamiliar)-3)*(answers_width/5);
    selection_rect=[xcenter-answers_width/10,answers_location(2),xcenter+answers_width/10,answers_location(4)] +[offset,0,offset,0];
    Screen('PutImage',w,imgArrays{trial}); % display item
    Screen('TextSize',w, 40);
    Screen('PutImage',w, familiarityQuestion_image,question1_location); % display question
    Screen('PutImage',w,familiarityAnswers_image,answers_location); % display answers
    Screen('FrameRect', w, [0,255,0], selection_rect, 5);
    Screen(w,'Flip');
    WaitSecs(0.5);
    
    %-----------------------------------------------------------------
%     % show fixation ITI
%     Screen('TextSize',w, 60);
%     Screen('DrawText', w, '+', xcenter, ycenter, white);
%     Screen(w,'Flip');
%     WaitSecs(1);

    KbQueueFlush;    
    
    
    % Go\NoGo question
    Screen('PutImage',w, imgArrays{trial}); % display item
    Screen('TextSize',w, 40);
    Screen('PutImage',w, goNoGoQuestion_image,question2_location); % display question2
    Screen('PutImage',w, goNoGoAnswers_image,answers_location); % display answers
    Screen(w,'Flip');
    
    KbQueueStart;
    %-----------------------------------------------------------------
    % get response
    
    
    noresp = 1;
    while noresp
        % check for response
        [keyIsDown, firstPress] = KbQueueCheck(-1);
        
        if keyIsDown && noresp
            findfirstPress = find(firstPress);
            tmp = KbName(findfirstPress);
            if ischar(tmp)==0 % if 2 keys are hit at once, they become a cell, not a char. we need keyPressed to be a char, so this converts it and takes the first key pressed
                tmp = char(tmp);
            end
            response_isGo = tmp(1);
            if response_isGo==key1||response_isGo==key2||response_isGo==key3||response_isGo==key4||response_isGo==key5 % A valid response is only 1,2,3,4 or 5
            noresp = 0;
            end
            
        end % end if keyIsDown && noresp
        
    end % end while noresp
    

      %-----------------------------------------------------------------
    % redraw text output with the appropriate colorchanges to highlight
    % response
    offset=(str2double(response_isGo)-3)*(answers_width/5);
    selection_rect=[xcenter-answers_width/10,answers_location(2),xcenter+answers_width/10,answers_location(4)] +[offset,0,offset,0];
    Screen('PutImage',w,imgArrays{trial}); % display item
    Screen('TextSize',w, 40);
    Screen('PutImage',w, goNoGoQuestion_image,question2_location); % display question
    Screen('PutImage',w,goNoGoAnswers_image,answers_location); % display answers
    Screen('FrameRect', w, [0,255,0], selection_rect, 5);
    Screen(w,'Flip');
    WaitSecs(0.5);
      
    
    %-----------------------------------------------------------------
    % show fixation ITI
    Screen('TextSize',w, 60);
    Screen('DrawText', w, '+', xcenter, ycenter, white);
    Screen(w,'Flip');
    WaitSecs(1);
    
    %-----------------------------------------------------------------
   
    KbQueueFlush;
    
end % end loop for trial = 1:length(food_images);

% End of session screen       
        Screen('TextSize',w, 40);
        CenterText(w,'Thank you!', white,0,-50);
        CenterText(w,'Questions?', white,0,50);
        Screen(w,'Flip');

% Closing
WaitSecs(4);
toc
ShowCursor;
Screen('CloseAll');

end % end function