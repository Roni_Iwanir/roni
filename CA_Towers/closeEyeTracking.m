function closeEyeTracking(edfFile, subjectID, runStart)
%%   Finishing eye tracking  %
    %---------------------------------------------------------------
    if use_eyetracker
        Task=GenFlags.Probe.str ; % update 
        %---------------------------
        % finish up: stop recording eye-movements,
        % close graphics window, close data file and shut down tracker
        Eyelink('StopRecording');
        %   Eyelink MSG
        % ---------------------------
        Eyelink('Message',['Eyetracking_closeTime: ',num2str(GetSecs-runStart)]);
        WaitSecs(.1);
        Eyelink('CloseFile');
        
        
        % download data file
        try
            fprintf('Receiving data file ''%s''\n', edfFile );
            status=Eyelink('ReceiveFile');
            if status > 0
                fprintf('ReceiveFile status %d\n', status);
            end
            if 2==exist(edfFile, 'file')
                fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, pwd );
            end
        catch rdf
            fprintf('Problem receiving data file ''%s''\n', edfFile );
            rdf;
        end
        
        
        if dummymode==0
            movefile(edfFile,['./Output/', subjectID,'_',Task,'_eyetracking_', timestamp,'.edf']);
        end;
    end
KbQueueFlush;