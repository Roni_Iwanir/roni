function CountLadderBelowThresh = FalseAlarm (Data)

Data=Data(Data(:,9)>=999000,:); % Use only NoGO trials
RT=Data(:,7);

CountLadderBelowThresh=sum(RT<=1500)/length(RT); %