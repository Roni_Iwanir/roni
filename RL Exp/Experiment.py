#!/usr/bin/env python

from Tasks import *
import ScreenUtilities
from enum import Enum
import logging
import Exceptions
#import BDMresolve
import random
import OutputWriter
import os
import ParamsParser
import ExtractDataFromOutput
import  pyspantask

PARAM_FILE = "paramsFile.ini"


class Experiment:

    def __init__(self, subject):
        self.__subject = subject
        self.__paramsFile = PARAM_FILE
        self.response = ""
        self.baseDir = ParamsParser.getParamValueByName(self, "basedir")
        self.expDir = self.baseDir + "/outputs/"+str(self.__subject.getId())
        self.outputPrefix = ParamsParser.getParamValueByName(self, "expPrefix")

        os.system("mkdir " + self.expDir)
        logFileName = self.expDir + "_logFile.txt"
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename=logFileName, level=logging.INFO )

        self.logger = logging.getLogger(__name__)
        OutputWriter.createPersonalDetailsFile(self.expDir , self.__subject)
        ExtractDataFromOutput.BaseDir= self.baseDir


    def run(self):
        self.logger.info(" ** START EXPERIMENT** ")
        startingInstructionFileName = self.baseDir + "/" + ParamsParser.getParamValueByName(self, "startinginsructions")

        baselineBandit = Task(self, TaskTypes.BANDIT)
        firstNback = Task(self, TaskTypes.NBACK,runs=5) #nback
        secondBandit = Task(self, TaskTypes.BANDIT)
        secondNback= Task(self, TaskTypes.NBACK,runs=5)
        thirdBandit = Task(self, TaskTypes.BANDIT)

        #consolidationInstructionFileName = self.baseDir + "/" + ParamsParser.getParamValueByName(self, "consoliationinstructions")

        #updateRewardTrialsForValueTask(valueTrainingTask)

        self.logger.info(" ** RUN TASKS ** ")

        try:

            ScreenUtilities.displayIntructions(self.window, startingInstructionFileName)
            baselineBandit.run(prefix=TaskTypes.BANDIT[0], TimeNum='first')

            firstNback.run(prefix=str(sys.argv[5])+'nback',TimeNum='first')
            secondBandit.run(prefix=TaskTypes.BANDIT[0], TimeNum='second')
            secondNback.run(prefix=str(sys.argv[5])+'nback',TimeNum='second')
            thirdBandit.run(prefix=TaskTypes.BANDIT[0], TimeNum='third')
            #pyspantask()


        except Exceptions.exitExperimentException as e:
            raise e
        self.logger.info(" ** FINISH EXPERIMENT** ")

        print "Finish exp , go to Bonus respolve"


    def initWindow(self):
        try:
            width = int(ParamsParser.getParamValueByName(self, "width"))
            height = int(ParamsParser.getParamValueByName(self, "height"))
            self.window = ScreenUtilities.defineInitialWindow(width,height)
            self.logger.info("window initilized")
        except:
            self.window = None
            self.logger.error( "couldnt initilize window")

    def getWindow(self):
        return self.window

    def getSubjectId(self):
        return self.__subject.getId()

    def getParamsFile(self):
        return self.__paramsFile

    def isIdExists(self, subjectId):
        listOfIdsFileName = self.baseDir + "/outputs/listOfIds"
        f = file(listOfIdsFileName).read()
        for currId in f.split():
            # do something with word
            if currId.strip() == str(subjectId).strip():
                return True

        os.system("echo " + str(subjectId) + " >> " + listOfIdsFileName)



