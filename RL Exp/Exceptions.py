class BadInput(RuntimeError):
    def __init__(self, msg):
        self.msg = msg

class KbNotPressedException(RuntimeError):
    def __init__(self, msg):
        self.msg = msg


class quitKBException(RuntimeError):
    def __init__(self, msg):
        self.msg = msg

class exitExperimentException(RuntimeError):
    def __init__(self, msg):
        self.msg = msg
