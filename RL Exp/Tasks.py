#!/usr/bin/env python
from enum import Enum

import TrialGenerator
import CasinoGenerator
import sys
import sysconfig
import ScreenUtilities
import OutputWriter
import ParamsParser, Stimuli, Trial
import random, math
from random import *
from collections import Counter
import numpy

from bisect import bisect

import os
from random import shuffle, gauss
import Exceptions
import TrialsExecutor

class TaskTypes(Enum):
    NBACKDEMO="nbackdemo"
    NBACK = "nback",
    BANDIT = "bandit",



    def __str__(self):
        return self.value[0]



class Task:

    def __init__(self, experiment, taskType, runs = 1, withDemo=True):

        self.experiment = experiment
        self.taskType = taskType
        # self.taskType = "auction"

        self.taskParams = ParamsParser.getParamsListBySection(experiment, self.taskType[0])
        #self.taskDir = ParamsParser.getParamValueByName(experiment, "basedir") + ParamsParser.getParamValueByName(experiment, self.taskType[0])
        self.numOfDemoTrials=int(self.taskParams["numofdemotrials"])
        self.numOfTrials=int(self.taskParams["numoftrials"])

        #stimuliDirForTrials = self.taskDir + ParamsParser.getParamValueByName(self.experiment, "stimulidir")
        self.listOfTrialsForDemo = self.__generateListOfStimuli(experiment, self.taskType, self.numOfDemoTrials)
#
        self.TakeBreak=ParamsParser.getParamValueByName(experiment, "basedir")+'/'+ ParamsParser.getParamValueByName(self.experiment, "break")
        self.EndOfTask=ParamsParser.getParamValueByName(experiment, "basedir")+'/'+  ParamsParser.getParamValueByName(self.experiment, "EndOfTask")
       # stimuliDirForDemo = self.taskDir + ParamsParser.getParamValueByName(self.experiment, "demodir")
        #self.listOfTrialsForDemoN = self.__generateListOfTrials(stimuliDirForDemo)

        self.runs = runs

    def runDemo(self,prefix):


        self.printInstructions(prefix)

        toRunDemo = True
        while toRunDemo:
            ScreenUtilities.addText(self.experiment.window, "DEMO")
            ScreenUtilities.flipWindow(self.experiment.window, seconds=2)
            DemoRes=TrialsExecutor.run(self.listOfTrialsForDemo)
            if prefix[1:]== TaskTypes.NBACK[0]: self.displayFeedback(DemoRes)
            toRunDemo = ScreenUtilities.toRunDemo(self.experiment, self.experiment.window)

    def runTask(self, prefix):

        self.experiment.logger.info( " ** START TRIALS **")
        self.printInstructions(prefix)
        # cross = self.displayFixationCross()
        totalTaskResponse = []
        for currRun in range(0,self.runs):
            self.experiment.logger.info( "Run number %s ", currRun )
            self.listOfTrials = self.__generateListOfStimuli(self.experiment, self.taskType, self.numOfTrials)
            #shuffle(self.listOfTrialsNback)
            runResponse = TrialsExecutor.run(self.listOfTrials)
            totalTaskResponse.append(runResponse)
            if prefix[1:]==TaskTypes.NBACK[0]:
                self.displayFeedback(runResponse)
            if currRun !=self.runs-1:
                ScreenUtilities.displayIntructions(self.experiment.getWindow(),self.TakeBreak)
            else: ScreenUtilities.displayIntructions(self.experiment.getWindow(),self.EndOfTask)

        # self.stopFixtaionCross(cross)
        self.experiment.logger.info( " ** END TRIALS ** ")

        return totalTaskResponse

    def run(self, prefix = "", TimeNum=""):

        self.experiment.logger.info("Task Is %s - %s " %(self.taskType[0],sys.argv[5]))
        taskResponse = []

        try :

            self.runDemo(prefix)
            taskResponse = self.runTask(prefix)

        except Exceptions.quitKBException as e:
            self.experiment.logger.info(e.msg)
            self.experiment.logger.info("User pressed Q - skip task")


        self.writeResponseToFile(taskResponse, prefix, TimeNum)

    def writeResponseToFile(self, taskResponse, prefix = "", TimeNum=""):
        self.experiment.logger.info( " ** WRITE TO FILE **")
        subjectId = self.experiment.getSubjectId()
        dir = self.experiment.baseDir + "/outputs/"+str(subjectId)
        os.makedirs(dir) if not os.path.exists(dir) else None
        outputFileName = dir +  "/" + self.experiment.outputPrefix + "-subject#" + str(subjectId) + "_output_" +prefix+"-"+ TimeNum+'_'+self.taskType[0] + ".csv"
        self.experiment.logger.info("file is %s" %(outputFileName))
        header = self.taskParams["outputfileheader"] +"\n"
        OutputWriter.exportToFile(subjectId, outputFileName, header, taskResponse)

    def printInstructions(self, prefix=""):

        instructionFileName = ParamsParser.getParamValueByName(self.experiment, "instructionfilenamesuffix")
        instructionFile = self.experiment.baseDir + "/" + self.taskType[0] + '/' + prefix + instructionFileName
        ScreenUtilities.displayIntructions(self.experiment.window, instructionFile)

    def displayFixationCross(self):
        return ScreenUtilities.displayFixationCross(self.experiment.window)

    def stopFixtaionCross(self, cross):
        ScreenUtilities.stopFixtaionCross(self.experiment.window, cross)

    def __generateListOfTrials(self, stimuliDir):

        listOfAllStimuliPerTrialTuple = self.__generateTuplesOfStimuliPerTrial(stimuliDir)
        [listOfAlslStimuliPerTrialTuple.remove(x) for x in listOfAllStimuliPerTrialTuple if x==[]]
        numOfTrialPerStimuli = int(self.taskParams["numoftrialsperstimuli"])
        listOfAllStimuliPerTrialTuple *= numOfTrialPerStimuli


        listOfTrials = []
        trialIndex = 0
        for stimuliTuple in (listOfAllStimuliPerTrialTuple ):

            singleTrialStimuli = []
            stimuliPosIndex = 0
            for fileName in stimuliTuple:
                xPos = int(self.taskParams["xpos"].split(",")[stimuliPosIndex])
                yPos = int(self.taskParams["ypos"].split(",")[stimuliPosIndex])

                currentStimuli = Stimuli.Stimuli(fileName, xPos, yPos, trialIndex)
                singleTrialStimuli.append(currentStimuli)
                stimuliPosIndex +=1

            currentTrial = Trial.Trial(self.experiment, self.taskType , singleTrialStimuli, trialIndex)
            listOfTrials.append(currentTrial)
            trialIndex +=1

        return listOfTrials

    def __generateListOfStimuli(self,experiment, taskType, numOfTrials):
        if taskType[0]=='nback':
            return self.__generateListOfStibmuliForNBack(experiment,taskType, int(sys.argv[5]),numOfTrials)
        elif taskType[0]=='bandit':
            return self.__generateListOfTrialsBandit(experiment,taskType,numOfTrials)

    def __generateListOfStibmuliForNBack(self,experiment, taskType, wmType, numOfTrials):
        trialIndex=0
        trialList=[]
        forbidden=[]
        numOfTargets=round(0.2*numOfTrials)
        if wmType==3:
            numOfLures=round(0.15*numOfTrials)
        else: numOfLures=0
        numOfFillers=numOfTrials-numOfTargets-numOfLures
        randBinList = lambda n: [randint(0, 1) for b in range(1, n + 1)]
        MakeLower = randBinList(numOfTrials)
        letters= self.taskParams["letters"].strip().split(",")

        trialTypeList = ['Filler','Filler','Filler']
        trialGen = TrialGenerator.TrialGenerator(numOfFillers-len(trialTypeList)+1,numOfLures,numOfTargets,letters)
        for i in range(2,numOfTrials-1):
            newTrialType = trialGen.randTrial()
            trialTypeList.append(newTrialType)
            trialGen.updateWeights(newTrialType)
        for trial in range(0,numOfTrials):
            stim = trialGen.pickChar(trialTypeList[trial], wmType, trial, forbidden)
            if trial<3:
                forbidden.insert(trial%3,stim)
            else:
                forbidden[trial%3]=stim
            if MakeLower[trial]==0:
                stim=stim.lower()
            if numOfTrials<20:
                newTrial= Trial.Trial(self.experiment, self.taskType ,  [stim], trialIndex, trialTypeList[trial],wmType, isDemo = True)
            else:
                newTrial = Trial.Trial(self.experiment, self.taskType ,  [stim], trialIndex, trialTypeList[trial],wmType )
            trialList.append(newTrial)
            trialIndex+=1

        return  trialList

    def __generateListOfTrialsBandit(self,experiment, taskType, numOfTrials):
        stimuliFolders=[]
        trialIndex = 0
        machines= self.taskParams["slots"].replace(" ","").split(",")
        for slot in machines: stimuliFolders.append(experiment.baseDir + "/" + taskType[0] + '/' + slot + 'Slot')
        listOfStimuli =map(os.listdir,stimuliFolders)
        BanditGen=CasinoGenerator.CasinoGenerator(self.taskParams,stimuliFolders)
        Rewards=BanditGen.randomWalkReward(numOfTrials)

        listOfTrials=[]
        for trialIndex in range(numOfTrials):
            listOfAllStimuliPerTrialTuple =BanditGen.slots(Rewards,trialIndex,listOfStimuli)
            if numOfTrials < 20:
                newTrial = Trial.Trial(self.experiment, self.taskType, listOfAllStimuliPerTrialTuple, trialIndex, trialType=int(sys.argv[1])%3,isDemo=True)
            else:
                newTrial = Trial.Trial(self.experiment, self.taskType, listOfAllStimuliPerTrialTuple, trialIndex, trialType=int(sys.argv[1])%3)  # todo:randimized per subject?
            listOfTrials.append(newTrial)


        return listOfTrials

    def __generateTuplesOfStimuliPerTrial(self, stimuliDir):

        self.experiment.logger.info( "** GENERATE LIST OF TRIALS **")
        listOfStimuliTuplePerTrial = []

        if self.taskParams["stimuligroupinsubfolders"] in ["False", "false"]:
            [listOfStimuliTuplePerTrial.append((stimuliDir + "/" + st,)) for st in os.listdir(stimuliDir) if st != ".DS_Store"]
            return listOfStimuliTuplePerTrial

        for dirName, subdirList, fileList in os.walk(stimuliDir):
            stimuliInDir = []
            for fname in fileList:
                if fname == ".DS_Store":
                    continue
                stimuliInDir.append(dirName + "/" + fname)

            #shuffle the tuple and make sure the core stimuli is last
            shuffle(stimuliInDir)
            stimuliInDir.sort(key = lambda x : "core" in x)
            listOfStimuliTuplePerTrial.append(stimuliInDir)

        return listOfStimuliTuplePerTrial


    def displayFeedback(self, runResponse):
        totalOutcome = []
        for runIndex, singleTrialResponse in enumerate(runResponse):
            totalOutcome += singleTrialResponse.getDictVal("outcome") + ","

        hitRate = round(((totalOutcome.count("e") / round(0.2 *len(runResponse)))), 2) * 100
        FA = totalOutcome.count("f")
        ScreenUtilities.addText(self.experiment.getWindow(), (
        "You pressed correctly about %i%% of the times, and %d times when you shouldnt have" % (hitRate, FA)))
        ScreenUtilities.flipWindow(self.experiment.getWindow(), 6)