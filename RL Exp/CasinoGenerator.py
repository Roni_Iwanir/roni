import random
import Stimuli

class CasinoGenerator:
    def __init__(self, params, stimuliFolders):
        self.params = params
        self.stimuliFolders = stimuliFolders

        self.initialMu = map(float,self.params["slotsstart"].replace(" ","").split(","))
        self.sigma = float(self.params["sdslots"])
        self.noiseSigma = float(self.params["noisesd"])
        self.noiseMean = float(self.params["noisemean"])
        self.decayParam = float(self.params["decayparam"])
        self.decayCenter = float(self.params["decaycenter"])


    def randomWalkReward(self,numOfTrials):
        possibleRewards=[]
        mu=self.initialMu[:]
        for i in range(0,numOfTrials):
            singleTrialReward=[]
            for slotmu in mu:
                reward=round(random.gauss(slotmu,self.sigma))
                if reward<0:
                    reward=0
                elif reward>100:
                    reward=100
                singleTrialReward.append(reward)
            mu=map(lambda x:x*self.decayParam+(1-self.decayParam)*self.decayCenter+random.gauss(self.noiseMean,self.noiseSigma),mu)
            possibleRewards.append(tuple(singleTrialReward))

        return   possibleRewards

    def slots(self,Rewards, trialIndex, listOfStimuli):
        singleTrialStimuli=[]
        for slotNum in range(len(self.stimuliFolders)):
            xPos = int(self.params["xpos"].split(",")[slotNum])
            yPos = int(self.params["ypos"].split(",")[slotNum])
            fileName=self.stimuliFolders[slotNum]+'/'+listOfStimuli[slotNum][0]
            currentStimuli = Stimuli.Stimuli(fileName, xPos, yPos, trialIndex, Rewards[trialIndex][slotNum])
            singleTrialStimuli.append(currentStimuli)


        return singleTrialStimuli
