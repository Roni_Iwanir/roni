
import GeneralUtilities

from enum import Enum

class ResponeStatus(Enum):

    INIT = "INIT",
    NO_USER_RESPONSE = "NO_USER_RESPONSE",
    PRESSED = "PRESSED",


class Response:
    def __init__(self, index):
        self.status = ResponeStatus.INIT
        self.__dict = {}
        self.__dict["notes"] = ""
        self.__dict["outcome"]=""
        self.__dict["index"] = index
        self.__asString = str(index) + "," +self.__dict["notes"]
        self.__csvCols = []

    def getDict(self):
        return self.__dict

    def getString(self):
        return self.__asString

    def getDictVal(self, key):
        return self.__dict[key]

    def setCsvCols(self, cols):
        self.__csvCols = cols

    def generateResponseStrForCsv(self):

        # self.__asString += ","
        for col in self.__csvCols:
            if self.__dict.has_key(col):
                self.__asString += str(self.__dict[col]) + ","
        self.__asString += str(self.__dict["notes"])

    def mergeResponse(self, newRes):
        if isinstance(newRes, str): #in case of error
            self.__dict["notes"] += newRes

        if isinstance(newRes, dict):
            self.__dict.update(newRes)


    def addToDict(self, key, value):
        a= self.getDict()
        a[key]=value


    def addGroupData(self, stimuliGroup):
        self.__asString += stimuliGroup.stimuliGroup[0].getGroupName()
        self.__asString += ","

    def addStimuliData(self, stimuliGroup):
        # type: (object) -> object
            try:
                self.__asString+=stimuliGroup+","
            except:
                for st in stimuliGroup:
                    self.__asString += st.getFileName().split("/")[-1] + ","


    def addSuccessNote(self,outcome):
        self.__asString +=outcome+","