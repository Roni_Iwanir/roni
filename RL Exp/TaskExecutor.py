

import TaskExecutorAbstract
import  Response
#self.experiment, window, self.taskType[0], self.stimuliGroup, self.trialType, isDemo, other

def nBackTaskExecutor(experiment, window, taskType, stimuliGroup, trialType, response, isDemo, other):

    nBackTask = TaskExecutorAbstract.NbackExecutor(experiment, taskType)
    nBackTask.displayFixation(window,isDemo)

    [res, trialRTClock]=nBackTask.displayStimuli(window,stimuliGroup[0],isDemo)

    newRes = nBackTask.getResponse(window, response,res, trialRTClock, isDemo)
    response.mergeResponse(newRes)
    #response.setCsvCols(nBackTask.responseCsvCols())

    if trialType=='Target':
        if newRes["kbPressed"]=='y':
            response.addToDict("outcome",'Correct')
        else:
            response.addToDict("outcome",'miss')
    else:
        if newRes["kbPressed"]=='y':
            response.addToDict("outcome",'fa')

    response.setCsvCols(nBackTask.responseCsvCols())

    return response

def banditTaskExecutor(experiment, window, taskType, stimuliTuple, trialType, response, isDemo, other):
    banditTask = TaskExecutorAbstract.BanditExecutor(experiment, taskType)
    trialRes = banditTask.displayStimuli(window, stimuliTuple, trialType, response, isDemo)
    banditTask.displayFixation(window,trialRes, isDemo)
    response.addToDict("outcome", 'Correct')
    response.addToDict("reward_0",stimuliTuple[0][0].value)
    response.addToDict("reward_1",stimuliTuple[0][1].value)
    response.addToDict("reward_2", stimuliTuple[0][2].value)
    response.mergeResponse(trialRes)

    response.setCsvCols(banditTask.responseCsvCols())
    return response

def featurerepresentationTaskExecutor(experiment, window, taskType, stimuliTuple, response, isDemo, other):
    frTask = TaskExecutorAbstract.FeatureRepresentationExecutor(experiment, taskType)
    newRes = frTask.displayStimuli(window, stimuliTuple, response, isDemo)
    response.addStimuliData(stimuliTuple)
    response.mergeResponse(newRes)
    response.setCsvCols(frTask.responseCsvCols())

    return response

def featuregenerationTaskExecutor(experiment, window, taskType, stimuliTuple, response, isDemo, other):
    fgTask = TaskExecutorAbstract.FeatureGenerationTask(experiment, taskType)
    response.mergeResponse(fgTask.displayStimuli(window, stimuliTuple, response, isDemo))
    response.setCsvCols(fgTask.responseCsvCols())

    return response