import random
from psychopy import visual, event
from enum import Enum

class BDM_COLUMNS(Enum):

    SUBJECT_ID = 0
    RUN = 1
    INDEX = 2
    GROUP = 3
    FILE_NAME = 4
    RESPONSE_TIME = 5
    SHEKELS_BIDDED = 6
    NOTES = 7



def resolve(experiment, auctionFileOutput):

    text = visual.TextStim(experiment.window, pos=(0,240), color='red', text= "Please call experimenter")
    text.draw()
    experiment.window.flip()
    event.waitKeys(9999, ['k'])

    auctionResponseFile = open(auctionFileOutput)
    lines = auctionResponseFile.readlines()

    indexOfBDMSnack =  random.randint(1, len(lines)-1)
    shekelsToWin = random.randrange(0,20,1) / 2

    selectedTrial = lines[indexOfBDMSnack].split(",")
    shekelsBidded = float( selectedTrial[BDM_COLUMNS.SHEKELS_BIDDED])

    isWon = shekelsBidded > shekelsToWin
    stimuli = selectedTrial[BDM_COLUMNS.GROUP]+ "-" + selectedTrial[BDM_COLUMNS.FILE_NAME]

    generateResolveOutputFile(auctionFileOutput, indexOfBDMSnack, shekelsBidded, shekelsToWin, isWon)
    writeToLog(experiment.logger, indexOfBDMSnack, shekelsBidded, shekelsToWin, isWon )

    if isWon:

        text = visual.TextStim(experiment.window, pos=(0,240), text= "You Won the snack:")


    else:
        text1 = visual.TextStim(experiment.window, text = "randomed snack is : %s" %(selectedTrial[BDM_COLUMNS.FILE_NAME].split("_")[0]), pos=(0, 240))
        text = visual.TextStim(experiment.window, text= "You can not buy it", pos=(0, -120))
        text1.draw()

    stimuliFile = experiment.baseDir + "/auction/stimuli/"+stimuli
    pic = visual.ImageStim(experiment.window,stimuliFile, size=(360,360), pos=(0,-240))
    pic.draw()
    text.draw()
    text2 = visual.TextStim(experiment.window, text= "Computer bid %.2f , and you bid %.2f" %(shekelsToWin, shekelsBidded), pos=(0, 120) )
    text2.draw()
    experiment.window.flip()
    event.waitKeys(10, ['space','q','esc'])

def generateResolveOutputFile(auctionFileOutput, indexOfBDMSnack, shekelsBidded, shekelsToWin, isWon):
    outputData = " randomed trial is #%d  , shekels bidded for this trial : %f ,shekels to win : %f .\n win is %s  " \
        %(indexOfBDMSnack, shekelsBidded, shekelsToWin, isWon)

    resolveOutputFileName = auctionFileOutput.replace("csv", "_resolve.txt")
    resolveFile = open(resolveOutputFileName, "w")
    resolveFile.write(outputData)
    resolveFile.close()

def writeToLog(logger, indexOfBDMSnack, shekelsBidded, shekelsToWin, isWon ):

    logger.info( "randomed trial is #%d" %(indexOfBDMSnack))
    logger.info( "shekels bidded for this trial : %f " %(shekelsBidded))
    logger.info( "shekels to win : %f " %(shekelsToWin))
    logger.info( "win %s " , isWon)