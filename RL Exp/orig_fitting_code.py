from random import normalvariate, choice, random
from math import *
from numpy import *
from time import sleep
from copy import deepcopy
from scipy.optimize import fmin

VERBOSE = False
NUM_FIT_ATTEMPTS = 10


# computes bayesian information criterion
def getBIC(LL, num_params, num_trials):
    return -2*LL + num_params*log(num_trials)

#takes 4 Q values and a temperature (exploitation parameter) and returns 4 action probabilties
def getChoiceProbabilities(Q, exploitation):
    choice_probs = zeros((3))
    for choice in range(3):
        numerator = exp( Q[choice] * exploitation)
        denominator = sum( exp(Q * exploitation) )
        choice_probs[choice] = numerator / denominator     
    return choice_probs

#your  Full Blown Kalman Filter model (from Daw et al 2006)
def getKalmanModelLikelihood (params, data):
    neg_log_likelihood = 0.0
    [exploit, lambda_param, theta, diff_noise,
     init_mean_1, init_variance_1,
     init_mean_2, init_variance_2,
     init_mean_3, init_variance_3] = params
    observation_noise = .02
    if( not ((0 < exploit < 10) and (0 < lambda_param < 1) and (0 < theta < 10)
             and (0 < diff_noise < 10) and
             (0 < init_mean_1 < 10) and (0 < init_variance_1 < 10) and 
             (0 < init_mean_2 < 10) and (0 < init_variance_2 < 10) and 
             (0 < init_mean_3 < 10) and (0 < init_variance_3 < 10))):

        return 10000
    Q = array( [init_mean_1, init_mean_2, init_mean_3])
    noise = array( [init_variance_1, init_variance_2, init_variance_3])
    for subj_trial in data:      
        subj_action = subj_trial['choice'] -1
        subj_reward = subj_trial['reward']
        response_probs = getChoiceProbabilities(Q, exploit)
        neg_log_likelihood += -1.0 * log( response_probs[subj_action])

        alpha = noise[subj_action] / (noise[subj_action] + observation_noise)
        Q[subj_action] += alpha * (subj_reward - Q[subj_action])
        noise[subj_action] *= (1-alpha)
        
        for choice in range(4):
            Q [choice]= lambda_param*Q[choice] + (1-lambda_param)*theta
            noise[choice] += (lambda_param**2)*noise[choice] + diff_noise
 
    return neg_log_likelihood

# Your simple RL Model
def getSoftmaxModelLikelihood(params, data):
    neg_log_likelihood = 0.0
    [alpha, exploit] = params
    if( (alpha < 0) or (alpha > 1) or (exploit < 0) or (exploit > 50)):
        return 10000
    Q = zeros((4))
    for subj_trial in data:      
        subj_action = subj_trial['choice'] -1
        subj_reward = subj_trial['reward']
        response_probs = getChoiceProbabilities(Q, exploit)
        neg_log_likelihood += -1.0 * log( response_probs[subj_action])
        Q[subj_action] += alpha * (subj_reward - Q[subj_action])
    return neg_log_likelihood


#this script assumes that the data file (data.csv) is a 3 column (subject number, choice, reward) CSV file-- that's all we need to fit the model
subjects = {}
trials = []
datafile = file('666.csv', 'r')
mylines = datafile.read()
trial_lines = mylines.split("\n")
for trial_line in trial_lines:
    subj_trial =  trial_line.split(',')
    try:
        [subj, choice, reward] = map(int, subj_trial)
    except:
        continue
    trials.append({'choice':choice, 'reward':float(reward)}) 
    if(subjects.has_key(subj)):
        subjects[subj]['trials'].append(subj_trial)
    else:
        subjects[subj] = {'trials' : [subj_trial]}

for subj_number, subject in subjects.iteritems(): 
    print 'subject:', subj_number

    print 'fitting simple RL model...'
    best_value = inf
    for fit_attempt in range(NUM_FIT_ATTEMPTS):
        results = fmin( getSoftmaxModelLikelihood, [random.uniform(), random.uniform()*10],
                        args=tuple([subject['trials']]), full_output=1)
        [params, LL] = results[0:2]
        if(LL < best_value):
            subject['softmax_fitted_params'] = params
            subject['softmax_LL'] = -1*LL

    
    print 'fitting full-blown kalman filter model...'
    best_value = inf
    for fit_attempt in range(NUM_FIT_ATTEMPTS):
        results = fmin(   getKalmanModelLikelihood,
                          [random.uniform()*10, random.uniform(), random.uniform()*10, random.uniform()*10, 
                           random.uniform()*10, random.uniform()*10,
                           random.uniform()*10, random.uniform()*10,
                           random.uniform()*10, random.uniform()*10,
                           random.uniform()*10, random.uniform()*10 ],
                          args=[subject['trials']], full_output=1, maxiter = 10000)
        [params, LL] = results[0:2]
        if(LL < best_value):
            subject['full_blown_kalman_fitted_params'] = params
            subject['full_blown_kalman_LL'] = -1*LL
            best_value = LL
