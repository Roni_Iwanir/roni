import random
import ParamsParser

class TrialGenerator:
    def __init__(self, nFiller, nLure, nTarget, letters):
        self.nFiller = nFiller
        self.nLure = nLure
        self.nTarget = nTarget
        self.letters= letters

    def randTrial(self):
        num=random.randrange(1,self.nFiller+self.nTarget+self.nLure,1)
        if num <= self.nLure:
            trialType='Lure'
        elif self.nLure < num <= self.nLure+self.nTarget:
            trialType = 'Target'
        else:
            trialType ='Filler'
        return trialType

    def updateWeights(self, newTrial):
        if newTrial=='Filler':
            self.nFiller-=1
        elif newTrial=='Lure':
            self.nLure-=1
        else: self.nTarget-=1

    def pickChar(self, trialType, wmType, trial,forbidden):
        allstim = self.letters
        possibleStim=allstim[:]
        if len(forbidden)>0:
            for j in set(forbidden):
                possibleStim.remove(j)
        stim=random.choice(possibleStim)
        if trialType=='Filler':
            return stim
        elif trialType=='Target':
            if wmType==3:
                stim=forbidden[trial%3]
            elif wmType==2:
                stim = forbidden[trial % 3-2]
            else:
                stim= forbidden[trial%3-1]
        else:
            stim = forbidden[trial % 3 - 2]
        return stim
