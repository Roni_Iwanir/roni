#!/usr/bin/env python
from psychopy import core, visual , event, sound # import some libraries from PsychoPy
import datetime
import Exceptions
import GeneralUtilities
from psychopy import prefs
prefs.general['audioLib'] = ['pyo']
from psychopy import sound, event
import RS
import os
import time

def defineInitialWindow(width, height, unit="pix"):
    return visual.Window(monitor="testMonitor", units=unit, fullscr=True, color='Black') #todo: change
    #return visual.Window(size = (width,  height) , monitor="testMonitor", units=unit, color='Black')

# display:

def displayFixationCross(window):
    cross = visual.TextStim(window, text= "+", height=50)
    cross.setAutoDraw(True)
    window.flip()
    return cross

def stopFixtaionCross(window, cross):
    cross.setAutoDraw(False)
    window.flip()

def displayIntructions(window, instructions):

    try:
        message = visual.ImageStim(window , instructions+'.jpg', pos = (0,0))
    except:
        message = visual.ImageStim(window , instructions+'.png', pos = (0,0))
    message.draw(window)

    window.flip()
    core.wait(1)
    pressedKeys = event.waitKeys(999, ['space'])




#components functions

def createGabor(window):
    return visual.ImageStim(window,"/Users/orenkobo/Desktop/ExperimentsData/gabor.png" , pos = (0,0), size = (150,150))

def addFrameForImages(images, window, picSize):
    frames = []
    [frames.append(addFrame(window, picSize, image.pos)) for image in images]
    return frames


def addFrame(window, picSize, picPos):
    frame = visual.Rect(window, height=picSize[1]+15, width=picSize[0]+15, pos=picPos , closeShape=True, lineColor='white', lineWidth=5 )
    return frame

def addImage(window, stimuli, picSize):
    imageName = stimuli.fileName
    image = visual.ImageStim(window,imageName , pos = (stimuli.xPos, stimuli.yPos), size = picSize)
    image.draw(window)
    return image

def addMovingImages(window,stimuliFolder, picSize, transTime, pos=(0,0)):
    for image in os.listdir(stimuliFolder):
        imageName=stimuliFolder+"/"+image
        pic=visual.ImageStim(window,imageName, pos=pos, size=picSize)
        pic.draw(window)
        flipWindow(window,transTime)


def addImages(stimuliTuple, window, picSize):
    images = []
    RTClock = core.Clock()
    RTClock.reset()

    [images.append(addImage(window, stimulus, picSize)) for stimulus in stimuliTuple]
    return images

def addDemoText(window, isDemo , pos=(0,300)):
    if isDemo:
        return addText(window, "DEMO ", pos = pos, color='red')
    return None

def addSound(experiment, window, timeToPlay, audioFile):
    # audioFile = beep-01a.wav
    # audio = sound.SoundPyo(value=22254,secs=timeToPlay, volume=1)
    audio = sound.Sound(secs=timeToPlay)
    return audio

def displayRatingScale(experiment, window, images, timeToWait, text = None):
    m = event.Mouse(win=window)
    m.setVisible(True)
    ratingScale = RS.RatingScale(window,  pos=(0, -360), tickMarks=['0','1','2','3','4','5','6','7','8','9','10'] , textColor='White', markerExpansion=0.5, showAccept=False, scale=None, maxTime=timeToWait, textSize=1 , low=0, high=10, mouseOnly = True, singleClick = True, markerStart=3, precision=100)

    while ratingScale.noResponse:
        if text:
            text.draw()
        [image.draw() for image in images]
        ratingScale.draw()
        window.flip()
        if keyPressed('q'): #quit task not experimeny
            quitTask(window)

        if keyPressed('escape'):
            quitExperimentScreen(experiment, window, exception = Exceptions.exitExperimentException("Pressed esc for quit experiment"))


    if ratingScale.timedOut:
        experiment.logger.error("Didnt pressed mouse")
        return addNoResponseScreen(window, GeneralUtilities.ResponeTypes.TIMEOUT_NO_MOUSE_CLICKED, addNoResponse=True)

    experiment.logger.info("chose %f " %(ratingScale.getRating()))
    m.setVisible(False)
    return ratingScale

def addFixationCross(window):
    cross = visual.TextStim(window, text= "+")
    cross.setAutoDraw(True)


def addText(window, text, pos = (0,0), color='white', height = 40, bold = True):
    textStim = visual.TextStim(window, bold= bold, text= text, pos = pos , color=color, height=height)
    textStim.draw(window)
    return textStim

def addNoResponseScreen(window, status, addNoResponseMsg,waitNoRespone = False, cross=False):
    if addNoResponseMsg:
        window.flip()
        waitSeconds(0.02)
        addText(window, status[1], (0,30), color='white')
        window.flip()
        waitSeconds(1)

    if waitNoRespone:
        window.flip()
        waitSeconds(1)
        window.flip()

    if cross:
        addText(window, 'X' ,pos=(0,0), color='red',height=1000)
        window.flip()
        waitSeconds(1.9)
        window.flip()

    return status


#kb function

def flipWindow(window, seconds):
    window.flip()
    waitSeconds(seconds)

def toRunDemo(experiment, window):
    window.flip()
    text = addText(window, 'enter to run demo again,\n other key to continue')
    window.flip()

    key = getKB(experiment, window, 9999)
    if key[0] == 'return': #return is enter
        return True
    else:
        return False

def toTakeBreak(experiment, window):
    window.flip()
    text = addImage(window, 'Now you may have a short break, please press any key to continue')
    window.flip()

    key = getKB(experiment, window, 9999)
    if bool(key[0]):
        return False

def EndOfTask(experiment, window):
    window.flip()
    text = addText(window, '')
    window.flip()

    key = getKB(experiment, window, 9999)
    if bool(key[0]):
        return False


def getKB(experiment, window, timeToWait, keyList = None, clock = False, addNoResponseMsg = True, waitNoRespone = False, cross=False):
    pressedKeysAndRT = event.waitKeys(timeToWait, keyList , clock)

    if pressedKeysAndRT == None:
        return addNoResponseScreen(window, GeneralUtilities.ResponeTypes.TIMEOUT_NO_KEY_PRESSED, addNoResponseMsg, waitNoRespone, cross)

    if keyPressed('q', pressedKeysAndRT):
        quitTask(window)

    if keyPressed('escape', pressedKeysAndRT[0][0]):
        quitExperimentScreen(experiment, window, exception = Exceptions.exitExperimentException("Pressed esc for quit experiment"))

    experiment.logger.info('pressed %s ' %(pressedKeysAndRT[0][0]))

    return pressedKeysAndRT

#mouse functions:



def setMousePositionToCenter(window):
    mouse = event.Mouse(visible=True,newPos=(0,0), win=window)
    return mouse

def getMouseClickInRect(window, mouse, rect):
    while not (mouse.isPressedIn(rect)):
        True

    loc = mouse.getPos()
    return loc

#general screen functions

def waitSeconds(time):
    core.wait(time)

def keyPressed(key, pressed = None):
    if pressed == None:
        return event.getKeys([key])
    return pressed[0][0] == key


def quitTask(window):
    addText(window, "SKIP TASK")
    window.flip()
    waitSeconds(1)
    exception = Exceptions.quitKBException("user quit task")
    raise exception

def quitExperimentScreen(experiment, window, exception = None):
    if exception:
        experiment.logger.error(exception.msg)
        print exception.msg
        raise exception


    experiment.logger.info( "closing experiment")
    window.close()
    # core.quit()




def getWords(experiment, window, images, minNumOfWords, textPos, demoText):
    chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    picNum = 0
    images[picNum].draw()
    # demoText.draw()

    # Loop until return is pressed
    text = visual.TextStim(window, text='', pos=textPos)
    feedBackResponse = visual.TextStim(window, text='enter 3 features!', pos= (0,-200), color='red')
    endTrial = False
    numOfWords = 0

    while not endTrial:
        # Wait for response...
        images[picNum].draw()
        if demoText:
            demoText.draw()
        response = event.getKeys()
        if response:
            char = response[0]
            feedBackResponse.setAutoDraw(False)
            # If backspace, delete last character
            if char == 'backspace':
                if text.text[-1] in (' ', " ", "\n", '\n'): #if we deleted entire words
                    numOfWords -= 1
                text.setText(text.text[:-1])

            elif char == 'down': #this is arrowdown
                text.text+= "\n \n \n"
                endTrial = True
                continue

            elif char == 'escape':
                quitExperimentScreen(experiment, window, exception = Exceptions.exitExperimentException("Pressed esc for quit experiment"))

            # If tab, end trial.
            elif char == 'tab':
                if (numOfWords == minNumOfWords) or (numOfWords == minNumOfWords-1 and text.text[-1] in chars):
                    picNum += 1
                    endTrial = True
                    continue

                elif numOfWords < minNumOfWords:
                    feedBackResponse.setAutoDraw(True)

                else:
                    raise Exceptions.BadInput("couldnt understand if num of features is ok")
            # Insert enter
            elif char == 'return':
                text.setText(text.text + '\n')
                if text.text.rsplit("\n")[-2] != '':
                    numOfWords+=1

            # Insert space
            elif char == 'space':
                text.setText(text.text + ' ')

            # Else if a letter, append to text:
            elif char in chars:
                text.setText(text.text + char)

        # Display updated text
        text.draw()
        window.flip()

    return text.text