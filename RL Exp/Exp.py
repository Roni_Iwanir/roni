#!/usr/bin/env python

import Experiment
import sys
class Subject:

    def __init__(self, id, age, gender, hand, wm):
        self.__id = id
        self.__age = age
        self.__gender = gender
        self.__hand = hand
        self.__wm= wm

    def getId(self):
        return self.__id

    def getAge(self):
        return self.__age

    def getGender(self):
        return self.__gender

    def getHand(self):
        return self.__hand

    def getWM(self):
        return self.__wm

def main():
    print("hello world")
    try:
        subjectId = sys.argv[1]
        age = sys.argv[2]
        gender  = sys.argv[3]
        hand = sys.argv[4]
        wm= sys.argv[5]

    except:
        print "please enter subject id , age, gender, dominant hand"
        exit()

    print "subject id is %s " %(subjectId)

    currentSubject = Subject(subjectId, age, gender, hand, wm)
    currentExperiment = Experiment.Experiment(currentSubject)
    if currentExperiment.isIdExists(subjectId):
        ok = yesNo("Subject id already exists. is it ok? continue? Y/N")
        if not ok:
            print "ENTER NEW ID"
            exit()

    currentExperiment.initWindow()
    currentExperiment.run()

   # currentExperiment.analyseOutput()

    print("bye world")

def yesNo(q):
    reply = str(raw_input(q+' (y/n): ')).lower().strip()
    if reply[0] == 'y':
        return True
    if reply[0] == 'n':
        return False
    else:
        return yesNo("Uhhhh... please enter ")
if __name__ == '__main__':
    main()