import Experiment
import Response
import Stimuli
import Exceptions

#import BDMresolve

import random
class Trial:

    def __init__(self, experiment, taskType, stim, index, trialType, TrialAttribute="", isDemo = False):
        self.index = index
        self.experiment = experiment
        self.trialType=trialType
        self.stimuliGroup  = Stimuli.SingleTrialStimuliGroup(self.experiment, [stim], taskType, trialType)
        self.taskType = taskType
        self.TrialAttribute=TrialAttribute
        self.isDemoTrial = isDemo
        self.other = {}

    def __str__(self):
        return "trial num %d " %(self.index)

    def run(self):
        self.experiment.logger.info("trial num %d " %(self.index))
        trialResponse = Response.Response(self.index)
        self.presentStimuli(trialResponse)

        if self.taskType == Experiment.TaskTypes.NBACK:
            trialResponse.addStimuliData(self.stimuliGroup.trialType)
            trialResponse.addToDict("notes",self.stimuliGroup.stimuliGroup[0][0])

        trialResponse.generateResponseStrForCsv()

        self.responsesList = trialResponse

    def presentStimuli(self,response):
        return self.stimuliGroup.present(self.experiment.getWindow(), response, self.isDemoTrial, self.other)


