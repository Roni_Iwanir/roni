import numpy as np
import pprint
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.tools as tls
import math
import MathUtilities
py.sign_in('orenslab', 'n0j055basd')
tls.set_credentials_file(username='orenslab', api_key='n0j055basd')


def histogramOfTotalSubjectsDelta(dRewarded, dNotRewarded):
    xAxis = ['core', 'brand', 'color','ingredient']
    steR = MathUtilities.calcSteList(dRewarded.values())
    steNR = MathUtilities.calcSteList(dNotRewarded.values())
    t1 = go.Bar(
            x=xAxis,
            y=[np.mean(dRewarded['core']), np.mean(dRewarded['brand']),  np.mean(dRewarded['color']), np.mean(dRewarded['ingredient'])],
            name = 'Rewarded',
            error_y=dict(
               type='data',
               array=steR ,
               visible=True,
            )
    )
    t2 = go.Bar(
            x=xAxis,
            y=[np.mean(dNotRewarded['core']), np.mean(dNotRewarded['brand']),  np.mean(dNotRewarded['color']), np.mean(dNotRewarded['ingredient'])],
            name = 'Not Rewarded',
            error_y=dict(
               type='data',
               array=steNR ,
               visible=True,
            )
    )
    layout = go.Layout(title = "Delta Bdm before and after reward",
                       xaxis=dict(title='Dimenstion'),
                       yaxis=dict(title='delta bdm'),
                       barmode='group'
    )
    data = [t1,t2]
    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig, filename='grouped-bar')



def createHorizontalRatioBar(d):
    std = map(np.std, d.values())
    n = math.sqrt(len(d.values()))
    ste = [x / n for x in std]
    bars = go.Bar(
            y=map(np.mean,d.values()),
            x=map(str,d.keys()),
            name = "correction ration",
            error_y=dict(
               type='data',
               array=ste ,
               visible=True,
            )
    )

    layout = go.Layout(title = "Categorization correction ratio",
                       xaxis=dict(title='correction ratio'),
                       yaxis=dict(title='run number'),
                       )

    data = [bars]
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='correction ratio')

def scatterPlotWithSizeimension(d):
    #x is delta sim , y is delta bdm, color bar is similarity

    rewardedData = {}
    notRewardedData = {}
    for snack in d:
        rewardedData[snack] = d[snack]['rewarded']
        notRewardedData[snack] = d[snack]['Not rewarded']


    xAxisR = []
    yAxisR = []
    sizeAxisR = []
    for t in rewardedData.values():
        xAxisR.append(np.mean(t["brand-bdm-Delta"]))
        yAxisR.append(np.mean(t["brand-core_similarityDelta"]))
        sizeAxisR.append(np.mean(t["brand-core_similarityProbe"]) * 5)

    xAxisNR = []
    yAxisNR = []
    sizeAxisNR = []
    for t in notRewardedData.values():
        xAxisNR.append(np.mean(t["brand-bdm-Delta"]))
        yAxisNR.append(np.mean(t["brand-core_similarityDelta"]))
        sizeAxisNR.append(np.mean(t["brand-core_similarityProbe"]) * 5)


    traceRewarded = go.Scatter(
        y = xAxisR,
        x = yAxisR,
        # size = d["brand-core_similiriyProbe"] * 10
        mode='markers',
        marker=dict(
            size=sizeAxisR,
            color = 'rgba(255, 182, 193, .9)'
            # color=(0,255,0)
            )
        )

    traceNotRewarded = go.Scatter(
        y = xAxisNR,
        x = yAxisNR,
        # size = d["brand-core_similiriyProbe"] * 10
        mode='markers',
        marker=dict(
            size=sizeAxisNR,
            color = 'rgba(152, 0, 0, .8)'
            # color=(0,255,0)
            )
        )
    layout = go.Layout(title = "",
                       xaxis=dict(title='delta similarity'),
                       yaxis=dict(title='delta bdm'),
                       )

    data = [traceRewarded, traceNotRewarded]
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='scatter-plot-with-colorscale')

