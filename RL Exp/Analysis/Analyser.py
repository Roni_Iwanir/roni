#!/usr/bin/env python
import CsvToVos

class VOsGenerator:
    def __init__(self, expDir , subjectId):
        self.subjectId = subjectId
        self.expDir = expDir + "/" + str(subjectId)

        self.bdmBaselineCSVFile = self.getName(fileSuffix= "output_baseline-auction")
        self.bdmProbeCSVFile =  self.getName(fileSuffix= "output_probe-auction")
        self.frBaselineCSVFile =  self.getName(fileSuffix= "output_baseline-featurerepresentation")
        self.frProbeCSVFile =  self.getName(fileSuffix= "output_probe-featurerepresentation")
        self.catTrainingCSVFile =  self.getName(fileSuffix= "output_categorization-training")
        self.valueTrainingCSVFile =  self.getName(fileSuffix= "output_training-value")

        self.bdmHeader = "subjectId,run,index,group,File Name,Response Time,Shekels Bidded,notes"
        self.valueTrainingHeader = "subjectId,run,index,group,stimulus,kb pressed,RT,notes"
        self.catHeader ="subjectId,run,index,group,Stimulus left,Stimulus middle,Stimulus right,core stimuli,Kb Pressed,Response Time,chosen,notes"
        self.catTrainingHeader = "subjectId,run,index,group,Stimulus left,Stimulus middle,Stimulus right,core stimuli,Kb Pressed,Response Time,chosen,is correct,notes"

    def getName(self, fileSuffix ):
        return self.expDir + "/CAT_snacks-subject#" + str(self.subjectId) + "_" + fileSuffix + ".csv"

    def generateVOs(self):
        bdmBaselineVO = CsvToVos.csvTolistOfRows(self.bdmBaselineCSVFile)
        bdmProbeVO = CsvToVos.csvTolistOfRows(self.bdmProbeCSVFile)
        bdmFRBaselineVO = CsvToVos.csvTolistOfRows(self.frBaselineCSVFile)
        bdmFRProbeVO = CsvToVos.csvTolistOfRows(self.frProbeCSVFile)
        bdmCatTrainingVO = CsvToVos.csvTolistOfRows(self.catTrainingCSVFile)
        bdmValueTrainingVO = CsvToVos.csvTolistOfRows(self.valueTrainingCSVFile)

        return {"bdmBaselineVO" : bdmBaselineVO, "bdmProbeVO": bdmProbeVO, "bdmFRBaselineVO": bdmFRBaselineVO, "bdmFRProbeVO": bdmFRProbeVO,
                "bdmValueTrainingVO": bdmValueTrainingVO, "bdmCatTrainingVO": bdmCatTrainingVO }