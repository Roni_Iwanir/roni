

import csv
#import sys
# reader = csv.reader(open(sys.argv[0], "rb"))
def csvTolistOfRows(csvFileName):
    listOfRows = []
    csvDataFile = csv.DictReader(open(csvFileName))
    for row in csvDataFile:
        listOfRows.append(row)

    return listOfRows
