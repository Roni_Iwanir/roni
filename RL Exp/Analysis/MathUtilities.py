import numpy as np
import math

def calcSteList(l):
    std = map(np.std, l)
    sqrtN = math.sqrt(len(l[0]))
    ste = [x / sqrtN for x in std]
    return ste

def deltaFunc(baseline , probe):
    return probe - baseline
