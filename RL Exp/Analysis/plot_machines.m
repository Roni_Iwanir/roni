sub_vec=101:154;
disqualify_subs= [112 118 122 125 139 148]; %update
del=disqualify_subs-100;


valid_subs=sub_vec;
valid_subs(del)=[];
sunmerisedBanditData=zeros(length(sub_vec),6);
for sub=valid_subs
    banditFiles=dir(strcat('C:\Users\Tali\Dropbox\experimentsOutput\RL\outputs\', num2str(sub),'\','*bandit.csv'));
    for ind=1:length(banditFiles)
        f=fopen(strcat('C:\Users\Tali\Dropbox\experimentsOutput\RL\outputs\', num2str(sub),'\',banditFiles(ind).name));
        TempData=textscan(f, '%f %f %f %s %f %f %f %f %f %f %s','Delimiter',',','HeaderLines',1);
        fclose(f);
        plot(1:length(TempData{7}),TempData{7})
        hold on
        plot(1:length(TempData{7}),TempData{8})
        hold on
        plot(1:length(TempData{7}),TempData{9})
        scatter(1:length(TempData{7}),TempData{6},'k', 'filled')
        saveas (gcf,strcat('C:\Users\Tali\Dropbox\experimentsOutput\RL\ForAnalysis\frog_plots\',num2str(sub),'_',num2str(ind),'.jpg') );
        close all
    end
end
s=4;