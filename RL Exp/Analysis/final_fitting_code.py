import os
from random import normalvariate, choice, random
from math import *
from numpy import *
import numpy
from time import sleep
from copy import deepcopy
from scipy.optimize import fmin
from scipy.stats import gamma, beta, norm

VERBOSE = False
NUM_FIT_ATTEMPTS = 10

def getChoiceProbabilities(Q, exploitation):
    choice_probs = zeros((3))
    for choice in range(3):
        numerator = exp( Q[choice] * exploitation)
        denominator = sum( exp(Q * exploitation) )
        choice_probs[choice] = numerator / denominator
    return choice_probs



# Your simple RL Model
def getSoftmaxModelLikelihood(params, data):
    neg_log_likelihood = 0.0
    [alpha, exploit] = params
    if( (alpha < 0) or (alpha > 1) or (exploit < 0) or (exploit > 50)):
        return 10000
    Q = zeros((3))
    for subj_trial in data:
        subj_action = subj_trial['choice'] -1
        subj_reward = subj_trial['reward']
        response_probs = getChoiceProbabilities(Q, exploit)

#        print Q, response_probs, subj_action, subj_reward

#         if math.isnan(log( response_probs[subj_action])):
#             response_probs[subj_action]=1
        neg_log_likelihood += -1.0 * log( response_probs[subj_action])
        Q[subj_action] += alpha * (subj_reward - Q[subj_action])

    return neg_log_likelihood  - log( beta.pdf(alpha, 1.5, 1.5))  # - log(norm.pdf(exploit, .5, 1))# -1*log(gamma.pdf(exploit,2,1))


[firstSub,lastSub] = [101,154]

subjects = {}
path='C:/Users/Tali/Dropbox/experimentsOutput/RL/ForAnalysis/'
for filename in filter(lambda x:('.csv' in x) and ('_' not  in x), os.listdir(path)):
    trials = []
    datafile = file(path+filename, 'r')
    mylines = datafile.read()
    trial_lines = mylines.split("\n")

    subj = int(filename.split('.')[0])
    print filename

    for trial_line in trial_lines[4:]:
        trial =  trial_line.split(',')
        try:
            [ choice , reward, run_number] = map(int, trial)
        except: continue
        trials.append({'choice':choice, 'reward':int(reward), 'run':run_number})
    subjects[subj] = {'trials' :trials}

toWrite = "subj, run,alpha,beta,ll\n" #decay, bias, LL \n"
for subj_number, subject in subjects.iteritems():
    print 'subj:', subj_number

    subject['run_params'] = {1:[], 2:[], 3:[]}

    for run_number in [1,2,3]:
        print '\trun:', run_number
        run_trials = filter(lambda x:x['run']==run_number, subject['trials'])

        best_value = inf
        bet_params = [-1,-1]

        for fit_attempt in range(NUM_FIT_ATTEMPTS):
            results = fmin(getSoftmaxModelLikelihood, [numpy.random.beta(1.1,1.1), numpy.random.gamma(1.2,scale=5)],
                           args=(run_trials,), full_output=1)
            [params, LL] = results[0:2]
            if (LL < best_value):
                best_value = LL
                best_params = params

            subject['run_params'][run_number] = best_params

        print '\t', best_params
        toWrite += ','.join(map(str, [subj_number, run_number, best_params[0], best_params[1], best_value])) + '\n'

f = open('summary2.csv', 'w')
f.write(toWrite)
f.close()

