import Analyser, CsvToVos, Funcs, GraphsGenerator
from GroupAnalysis import Dimensions, GraphType
import GroupAnalysis

class SubjectAnalyser:
    def __init__(self, subjectId, listOfDims = GroupAnalysis.getKeys(Dimensions)):
        self.subjectId = subjectId
        self.expDir = "/Users/orenkobo/Desktop/ExperimentsData/outputs"
        self.analyser = Analyser.VOsGenerator(self.expDir , subjectId)
        self.vosDict = self.analyser.generateVOs()
        self.listOfDims = listOfDims
        self.resDict = {}
        self.outputFile = self.expDir + "/" + str(self.subjectId) + "/analyseFile.txt"
        print "ANALYSER OUTPUT"


    def runFuncs(self):
        funcs = Funcs.GeneralFuncs(self.outputFile)
        self.resDict[GraphType.TotalTaskDelta]= funcs.checkTaskDelta(csvBaseline = self.vosDict["bdmBaselineVO"], csvProbe = self.vosDict["bdmProbeVO"], listOfDims =  self.listOfDims)
        self.resDict[GraphType.CorrectionRatio] =  funcs.checkCorrectionRatio(self.vosDict["bdmCatTrainingVO"])

        self.resDict[GraphType.DeltaRewarded] = funcs.checkTaskDeltaFilterRewarded(csvValTraining = self.vosDict["bdmValueTrainingVO"], csvBaseline = self.vosDict["bdmBaselineVO"], csvProbe = self.vosDict["bdmProbeVO"], listOfDims = self.listOfDims, takeRewarded=True)
        self.resDict[GraphType.DeltaNotRewarded] = funcs.checkTaskDeltaFilterRewarded(csvValTraining = self.vosDict["bdmValueTrainingVO"], csvBaseline = self.vosDict["bdmBaselineVO"], csvProbe = self.vosDict["bdmProbeVO"], listOfDims = self.listOfDims, takeRewarded=False)
        self.resDict[GraphType.DeltaAsSimilarity] = funcs.bdmChangeAsSimilarityChange(self.vosDict["bdmBaselineVO"], self.vosDict["bdmProbeVO"], self.vosDict["bdmFRBaselineVO"], self.vosDict["bdmFRProbeVO"], self.vosDict["bdmValueTrainingVO"]) #- take only rewarwd vs not rewarded, take [similarity delta, bdm delta]

    def getRes(self):
        return self.resDict