import pprint
import MathUtilities
#

class GeneralFuncs:
    def __init__(self, outputFileName):
        self.outputFile = outputFileName
        self.outputData = ""

    def writeDataToFile(self):
        f = open(self.outputFile,'w')
        f.write(self.outputData)
        f.close()

    def checkTaskDeltaFilterRewarded (self, csvValTraining, csvBaseline, csvProbe, listOfDims, takeRewarded= True):

        filteredCsvBaseline = []
        filteredCsvProbe = []
        if takeRewarded == True:
            noteString = "rewarded"
        else:
            noteString = ""
        filteredTrials = filter(lambda x: x[" notes"] == noteString and int(x["run"])==0, csvValTraining)
        self.outputData += "Check only rewarded trials. there are %d rewarded. rewarded are: " %(len(filteredTrials))
        for x in filteredTrials:
            self.outputData += x["group"] + " , "
        self.outputData += "\n"

        for rewardedTrial in filteredTrials:

            coreFileName = rewardedTrial[" stimulus"]
            group = rewardedTrial["group"]

            filteredCsvBaseline.append(filter(lambda x: x[" File Name"] == coreFileName, csvBaseline)[0])
            filteredCsvBaseline.append(filter(lambda x: "brand" in x[" File Name"] and x[" group"] == group, csvBaseline)[0])
            filteredCsvBaseline.append(filter(lambda x: "ingredient" in x[" File Name"] and x[" group"] == group, csvBaseline)[0])
            filteredCsvBaseline.append(filter(lambda x: "color" in x[" File Name"].lower() and x[" group"] == group, csvBaseline)[0])
            filteredCsvProbe.append(filter(lambda x: x[" File Name"] == coreFileName, csvProbe)[0])
            filteredCsvProbe.append(filter(lambda x: "brand" in x[" File Name"] and x[" group"] == group, csvProbe)[0])
            filteredCsvProbe.append(filter(lambda x: "ingredient" in x[" File Name"] and x[" group"] == group, csvProbe)[0])
            filteredCsvProbe.append(filter(lambda x: "color" in x[" File Name"].lower() and x[" group"] == group, csvProbe)[0])

        return self.checkTaskDelta(filteredCsvBaseline, filteredCsvProbe, listOfDims)

    def checkTaskDelta(self,csvBaseline , csvProbe ,listOfDims): #task is bdm/training , dim is core / brand / ing / color
        baselineSum = 0
        probeSum = 0
        for x in csvBaseline:
            baselineSum += float(x["Shekels Bidded"])

        for x in csvProbe:
            probeSum += float(x["Shekels Bidded"])

        deltaList = {}
        for dim in listOfDims:
            deltaList[dim] = []

        csvBaseline.sort(key=lambda x: x[" group"])
        dimsDict = {}
        for dim in listOfDims:
            dimsDict[dim] = []

        for x in csvBaseline:
            stimDim = x[" File Name"].split("_")[1].split(".")[0].lower()
            group = x[" group"]
            baselineBid = float(x["Shekels Bidded"])
            probeBid = float(filter(lambda x: x[" group"] == group and stimDim in x[" File Name"].lower(), csvProbe)[0]["Shekels Bidded"])

            delta = MathUtilities.deltaFunc(baselineBid, probeBid)
            self.outputData += "for group %s , dim %s , delta (baseline - probe) is %.2f " %(group , stimDim, delta) + "\n"
            dimsDict[stimDim].append(delta)

        for dim in dimsDict:
            self.outputData += "sum of delta bdm in dim %s is %.3f" %(dim, sum(dimsDict[dim])) + "\n"

        return dimsDict

    def checkCorrectionRatio(self,csvCatTraining): #task is catTrainingTask or valTrainingTask
        totalCorrect = 0.0
        totalWrongs = 0.0
        ratioDict = {}
        for runAsKey in range(0,3):
            corrects = 0.0
            wrongs = 0.0
            runTrials =filter(lambda x: int(x["run"])==runAsKey, csvCatTraining)
            for x in runTrials:
                if x["is correct"] == "True":
                    corrects += 1
                else:
                    wrongs += 1
            
            self.outputData += "Run = %d percentages of corrects : %.3f" %(runAsKey, ( (corrects / (corrects + wrongs)) * 100)) + "\n"
            totalCorrect += corrects
            totalWrongs += wrongs
            ratioDict[runAsKey] = [( (corrects / (corrects + wrongs)) * 100)]
        self.outputData += "Total corrects : percentages of corrects : %.3f" %( ( (totalCorrect / (totalCorrect + totalWrongs)) * 100)) + "\n"
        return ratioDict

    def bdmChangeAsSimilarityChange(self,csvBdmBaseline, csvBdmProbe, csvFrBaseline, csvFrProbe, csvValTraining):

        listOfSimilarityBDM = []
        rewardedTrials   = filter(lambda x: x[" notes"] == "rewarded", csvValTraining)
        for valTrial in csvValTraining:
            if int(valTrial["run"]) > 0:
                break
            group = valTrial["group"]
            brandToCoreSimilarityBdm = float(filter(lambda x: x[" group"] == group and "brand" in x["Stimulus left"] +  x[" Stimulus right"]
                                                                                   and "core"  in x["Stimulus left"] +  x[" Stimulus right"], csvFrBaseline)[0][" Similarity Score"])
            brandToCoreSimilarityProbe = float(filter(lambda x: x[" group"] == group and "brand" in x["Stimulus left"] +  x[" Stimulus right"]
                                                                                   and "core"  in x["Stimulus left"] +  x[" Stimulus right"], csvFrProbe)[0][" Similarity Score"])
            brandToCoreSimilarityDelta = round(brandToCoreSimilarityBdm - brandToCoreSimilarityProbe, 2)
            bdmBrandBaseline = float(filter(lambda x: x[" group"] == group and "brand" in x[" File Name"], csvBdmBaseline)[0]["Shekels Bidded"])
            bdmBrandProbe = float(filter(lambda x: x[" group"] == group and "brand" in x[" File Name"], csvBdmProbe)[0]["Shekels Bidded"])
            bdmBrandDelta = round(bdmBrandBaseline - bdmBrandProbe, 2)
            res = {}
            delta = {}
            delta["brand-bdm-Delta"] = [bdmBrandDelta]
            delta["brand-core_similarityDelta"] = [brandToCoreSimilarityDelta]
            delta["brand-core_similarityProbe"] = [brandToCoreSimilarityProbe]
            delta["reward"] = valTrial in rewardedTrials
            res[group] = delta
            listOfSimilarityBDM.append(res)

        self.outputData += pprint.pformat(listOfSimilarityBDM) + "\n"

        return listOfSimilarityBDM
