
import random

import randint
import shuffle
from itertools import chain
from copy import copy

# generate stimulus list
# 1: create list of target letters (equal target prob each letter)
n = 3 # 3-back
Ntargets = 8
letters = ['B', 'F', 'K', 'H', 'M', 'Q', 'R', 'X']
SLt = letters*(Ntargets/8)
for i in range(40):
    shuffle(SLt)

# 2: create sequence & place target letters in it
listStimType = [ [['Standard'] + ['Target']] for i in range(Ntargets) ]
listStimType = list(chain(*listStimType))

listStim = [ [[''] + [t]] for t in SLt ]
listStim = list(chain(*listStim))

# 3: expand standards according to ITI
SLs = [randint(3,9) for i in range(Ntargets) ]
listStimType = [ [listStimType[i][0]]*SLs[i] + [listStimType[i][1]] for i in range(Ntargets) ]
listStimType = list(chain(*listStimType))

listStim = [ [listStim[i][0]]*SLs[i] + [listStim[i][1]] for i in range(Ntargets) ]
listStim = list(chain(*listStim))

# 4: expand targets according to n in n-back
for i in range(len(listStim)-n):
    if listStimType[i+n] == 'Target':
        listStim[i] = listStim[i+n]

# 5: assign remaining letters according to degrees of freedom
# do first couple

def sample(letters, listStim, n, i):
    global randint, copy
    x = copy(letters)
    for takeOut in range(1,n+1):
        try:
            x.remove( listStim[i+takeOut] )
        except ValueError: pass
    return x[ randint(0, len(x)) ]

for i in reversed(range(len(listStim)-n)):
    if listStim[i] == '':
        listStim[i] = sample(letters, listStim, n, i)
    else: continue

# export variables
self.experiment.items['trial_loop'].repeat = len(listStimType)
self.experiment.id = 0
self.experiment.listStimType = listStimType
self.experiment.listStim = listStim
self.experiment.isPrac = 1