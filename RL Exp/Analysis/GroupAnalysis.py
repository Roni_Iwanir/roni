import CatExpAnalyse
import GraphsGenerator
from enum import Enum

#todo: code review for all this package (all of output analyser)

class RewardedKey(Enum):
    Rewarded = "rewarded",
    NotRewarded = "Not rewarded"

class Dimensions(Enum):
    BRAND = "brand"
    COLOR = "color"
    ING   = "ingredient"
    CORE  = "core"

class GraphType(Enum):
    TotalTaskDelta = "totalTaskDelta"
    DeltaRewarded = "deltaRewarded"
    DeltaNotRewarded = "deltaNotRewarded"
    CorrectionRatio = "correctionRatio"
    DeltaAsSimilarity ="deltaAsSimilarity"

def getKeys(enumType):
    if enumType== GraphType:
        return [enumType.DeltaRewarded, enumType.DeltaNotRewarded, enumType.CorrectionRatio]
    if enumType== Dimensions:
        return [enumType.BRAND, enumType.COLOR, enumType.ING, enumType.CORE]

def main():
    listOfDims = getKeys(Dimensions)
    listOfSubjectIds = open("/Users/orenkobo/Desktop/ExperimentsData/outputs/listOfIds","r").read().replace(" ","").split("\n")

    results = {}
    for subjectId in listOfSubjectIds:
        if subjectId == "":
            continue
        subjectId = int(subjectId)
        singleSubjAnalyser = CatExpAnalyse.SubjectAnalyser(subjectId)
        singleSubjAnalyser.runFuncs()
        results[subjectId] = singleSubjAnalyser.getRes()

    groupData = getGroupData(results, listOfDims)
    createGraphs(groupData)


def createGraphs(data):
    #todo : check correctness of all 3 graphs
    # GraphsGenerator.scatterPlotWithSizeimension(data[GraphType.DeltaAsSimilarity])
    # GraphsGenerator.createHorizontalRatioBar(data[GraphType.CorrectionRatio]) #
    GraphsGenerator.histogramOfTotalSubjectsDelta(data[GraphType.DeltaRewarded], data[GraphType.DeltaNotRewarded])


def getGroupData(resultsDict, listOfDims):
    groupResults = {}
    groupResults[GraphType.DeltaAsSimilarity] = takeDeltaAsSimilarityAsRewardedGroupData(resultsDict)

    for key in getKeys(GraphType):
        groupResults[key] = {}
    for dim in listOfDims:
        groupResults[GraphType.DeltaRewarded][dim] = takeGroupDataAsListForDim(resultsDict, GraphType.DeltaRewarded, dim)
        groupResults[GraphType.DeltaNotRewarded][dim] = takeGroupDataAsListForDim(resultsDict, GraphType.DeltaNotRewarded, dim)
    for runNum in range(0,3):
        groupResults[GraphType.CorrectionRatio][runNum] = takeGroupDataAsListForDim(resultsDict, GraphType.CorrectionRatio, runNum)

    return groupResults

def takeDeltaAsSimilarityAsRewardedGroupData(resultsDict):
    groupResults = {}
    listOfAllSnacks = [x.keys()[0] for x in resultsDict.values()[0][GraphType.DeltaAsSimilarity]]
    isRewardedTypes = [GraphType.DeltaRewarded, GraphType.DeltaNotRewarded]
    typesOfData= ['brand-bdm-Delta',"brand-core_similarityDelta",'brand-core_similarityProbe' ]
    for snack in listOfAllSnacks:
        groupResults[snack] = {}

        for isRewarded in isRewardedTypes:
            groupResults[snack][isRewarded] = {}

            for dataType in typesOfData:
                groupResults[snack][isRewarded][dataType] = []

    for subject, results in resultsDict.items():

        for snackDict in results[GraphType.DeltaAsSimilarity]:
            currSnack = snackDict.keys()[0]
            currSnackResults = snackDict[currSnack]
            isRewarded = GraphType.DeltaRewarded if currSnackResults['reward'] == True else GraphType.DeltaNotRewarded
            for dataType in typesOfData:
                if dataType != 'reward':
                    groupResults[currSnack][isRewarded][dataType] += currSnackResults[dataType]

    return groupResults


def takeGroupDataAsListForDim(resultsDict, key, dim):
    allSubjectsResults = []
    for subject, results in resultsDict.items():
        allSubjectsResults += results[key][dim]

    return allSubjectsResults

if __name__ == '__main__':
    main()