disqualify_subs= [112 118 122 125 139 148]; %update
del=disqualify_subs-100;

DataPath='C:\Users\Tali\Dropbox\experimentsOutput\RL\ForAnalysis\analyzed\';

fid1=fopen([DataPath 'All_subjects_DATA_ross.csv']); %change! (1)

%1-Sub	 2-Run	3-Alpha	4-Beta	5-LL 6-N 7-Partial_load_Score	8-All_Or_None
paramData=textscan(fid1, '%f %f %f %f %f %f %s %s','Delimiter',',','HeaderLines',1);
fclose(fid1);

fid2=fopen([DataPath 'All_subjects_nBack.csv']);
%1-Sub	2-N	 3-block	 4-run	 5-Hits	 6-FA_Lure	 7-FA_filler 8-RT_target 9-RT_Lure	 10-RT_filler	 11-RT_FA	 12-Target_acc	13-Foil_acc	14-Lure_acc	 15-sensitivity (D_l) 16- Bias (C_l)
N_data=textscan(fid2, '%f %f %s %f %f %f %f %f %f %f %f %f %f %f %s %s','Delimiter',',','HeaderLines',1);
fclose(fid2);


baseline_vec=1:3:length(paramData{1,1});
sec_vec=2:3:length(paramData{1,1});
third_vec=3:3:length(paramData{1,1});

subs=paramData{1,1}(baseline_vec);
allOrNone=zeros(size(subs));
Partial_load=zeros(size(subs));

for ind=1:length(subs)
    if strcmp(paramData{1,7}{ind*3-1,1},'High');
        Partial_load(ind)=1;
    end
    if strcmp(paramData{1,8}{ind*3-1,1},'High');
        allOrNone(ind)=1;
    end
end

Partial_load(del)=[];
allOrNone(del)=[];


delta1=paramData{1,3}(sec_vec)-paramData{1,3}(baseline_vec);
delta1(del)=[];
delta2=paramData{1,3}(third_vec)-paramData{1,3}(sec_vec);
delta2(del)=[];
deltaTot= paramData{1,3}(third_vec)-paramData{1,3}(baseline_vec);
deltaTot(:,2)=paramData{1,6}(baseline_vec);
deltaTot(del,:)=[];

N_data_first3D=[]
N_data_second3D=[];
mat_n=cell2mat(N_data(1,[4:14])); % 4-run
for ind=1:5
    N_data_first3D(:,:,ind)=mat_n(ind:10:end,2:end);
    N_data_second3D(:,:,ind)=mat_n(ind+5:10:end,2:end);
    
end

%1-Hits	 2-FA_Lure	 3-FA_filler 4-RT_target 5-RT_Lure	 5-RT_filler	 6-RT_FA	 7-Target_acc	8-Foil_acc	9-Lure_acc
N_data_first3D(del,:,:)=[];
N_data_second3D(del,:,:)=[];


meanFirstN_Data=mean(N_data_first3D,3);

Counttrials1=sum(N_data_first3D,3);
hitsfirst=Counttrials1(:,1);
FA_first_tot=Counttrials1(:,2)+Counttrials1(:,3);

p_hit_first=(hitsfirst+0.5)/(201);
p_FA_first_tot=(FA_first_tot+0.5)/(201);
p_FA_first_Filler=(Counttrials1(:,3)+0.5)/(201);


d_p_first=icdf('normal',p_hit_first,0,1)-icdf('normal',p_FA_first_tot,0,1);
d_p_first_filler=icdf('normal',p_hit_first,0,1)-icdf('normal',p_FA_first_Filler,0,1);


matResfirst= [delta1 deltaTot d_p_first Partial_load allOrNone];

meanSecN_Data=mean(N_data_second3D,3);
Counttrials2=sum(N_data_second3D,3);
hitssec=Counttrials2(:,1);
FA_sec=Counttrials2(:,2)+Counttrials2(:,3);
p_hit_sec=(hitssec+0.5)/(201);
p_FA_sec=(FA_sec+0.5)/(201);
d_p_sec=icdf('normal',p_hit_sec,0,1)-icdf('normal',p_FA_sec,0,1);

matResSec= [delta2 deltaTot d_p_sec Partial_load allOrNone];

D1_High3=matResfirst(matResSec(:,6)==1&matResSec(:,3)==3,1);
D1_low3=matResfirst(matResSec(:,6)==0 &matResSec(:,3)==3,1);
D1_High1=matResfirst(matResSec(:,6)==1&matResSec(:,3)==1,1);
D1_low1=matResfirst(matResSec(:,6)==0 &matResSec(:,3)==1,1);

D2_High3=matResSec(matResSec(:,6)==1&matResSec(:,3)==3,1);
D2_low3=matResSec(matResSec(:,6)==0 &matResSec(:,3)==3,1);
D2_high1=matResSec(matResSec(:,6)==1&matResSec(:,3)==1,1);
D2_low1=matResSec(matResSec(:,6)==0 &matResSec(:,3)==1,1);


[R, P]= corr(delta1, meanFirstN_Data);
[h,p,ci,stats] = ttest2(D1_High1, D1_low1)
[h,p,ci,stats] = ttest2([D1_High3; D1_low3],[D1_High1; D1_low1])

[h,p,ci,stats] = ttest2([D2_High3; D2_low3],[D2_High1; D2_low1])

%%
sub_vec=101:154;
valid_subs=sub_vec;
valid_subs(del)=[];
sunmerisedBanditData=zeros(length(sub_vec),6);
for sub=valid_subs
    banditFiles=dir(strcat('C:\Users\Tali\Dropbox\experimentsOutput\RL\outputs\', num2str(sub),'\','*bandit.csv'));
    for ind=1:length(banditFiles)
        f=fopen(strcat('C:\Users\Tali\Dropbox\experimentsOutput\RL\outputs\', num2str(sub),'\',banditFiles(ind).name));
        TempData=textscan(f, '%f %f %f %s %f %f %f %f %f %f %s','Delimiter',',','HeaderLines',1);
        fclose(f);
        sunmerisedBanditData(sub-100,ind)=mean(TempData{10});
        sunmerisedBanditData(sub-100,ind+length(banditFiles))=std(TempData{10});

    end
end
sunmerisedBanditData(sunmerisedBanditData(:,1)==0,:)=[]
sunmerisedBanditData=[sunmerisedBanditData matResfirst(:,3)];
s=4;