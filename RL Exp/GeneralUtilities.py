
from enum import Enum

class ResponeTypes(Enum):

    TIMEOUT_NO_KEY_PRESSED = "Did not press any key" ,"Please press faster"
    TIMEOUT_NO_MOUSE_CLICKED = "Did not click the mouse","Please press the mouse faster"

def isCoreStimulusByFileName(fileName):
    return 'core' in fileName

def getCoreStimulusFromStimuliTuple(stimuliTuple):
    for st in stimuliTuple:
        if isCoreStimulusByFileName(st.getFileName()):
            return st

def removeStimulusFromTuple(coreStimulus, stimuliTuple):
    newStimuliTuple = []
    for a in list(stimuliTuple):
        if not a == coreStimulus:
            newStimuliTuple.append(a)

    return newStimuliTuple

