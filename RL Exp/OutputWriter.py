import ParamsParser
from pandas import DataFrame



def createPersonalDetailsFile(baseDir, subject):
    fileName = baseDir + "/personalDetails.txt"
    f = open(fileName,'a')
    data = "subject id , age , gender, hand, wm load "
    data += "\n"
    data += subject.getId() + ", " + subject.getAge() + "," + subject.getGender() + "," + subject.getHand()  +"," + subject.getWM() + "\n"

    f.write(data)
    f.close()

def exportToFile(subjectId, fileName, header, data):

    f = open(fileName,'w')

    toWrite = "subjectId"+"," + header
    for runIndex, singleRunRespone in enumerate(data):
        for singleTrialResponse in singleRunRespone:

            toWrite += str(subjectId) + "," +str(runIndex) + "," + singleTrialResponse.getString()
            toWrite += "\n"

    f.write(toWrite)
    f.close()