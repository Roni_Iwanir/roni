from abc import ABCMeta, abstractmethod

import ParamsParser, ScreenUtilities
import Stimuli
from psychopy import core
from psychopy import sound, event
import GeneralUtilities
from Response import ResponeStatus
import random
import time

def getKBIndex(validKeys, key):
    if isinstance(validKeys, list):
        return validKeys.index(key)
    else:
        return validKeys.replace(" ","").split(",").index(key)


def drawFrameForSelectedImage(res, images, frames, validKeys):
    if isinstance(res, tuple) and isinstance(res[0], str)  and isinstance(res[1], str):
        return
    index = getKBIndex(validKeys, res[0][0])
    frames[index].draw()
    images[index].draw()

def AnimateStim(res,images,validKeys,window):
    if isinstance(res, tuple) and isinstance(res[0], str) and isinstance(res[1], str):
        return
    window.flip()
    ScreenUtilities.waitSeconds(0.5)
    index = getKBIndex(validKeys, res[0][0])
    machineFolder="/".join(images[index]._imName.split("/")[0:-1])
    ScreenUtilities.addMovingImages(window, machineFolder,images[index].size.tolist(),0.005)
    window.flip()

class TaskExecutorAbstract():
    """
        This is an interface for all the tasks in the expriments.
    """
#self.experiment, window, self.taskType[0], self.stimuliGroup, self.trialType, isDemo, other
    __metaclass__ = ABCMeta

    @abstractmethod
    def displayStimuli(self, window, stimuliTuple, response, isDemo):
        raise NotImplementedError('subclasses must override displaystimuli()!')

    @abstractmethod
    def res2data(self, result, response):
        raise NotImplementedError('subclasses must override res2data()!')

    @abstractmethod
    def responseCsvCols(self):
        raise NotImplementedError('subclasses must override csvCols()!')

class NbackExecutor(TaskExecutorAbstract):

    def __init__(self, experiment,  taskType):
        self.experiment = experiment
        self.fixationTime = float(ParamsParser.getParamsListBySection(experiment, taskType)["fixationtime"])
        self.stimulusTime = float(ParamsParser.getParamsListBySection(experiment, taskType)["stimulustime"])
        self.timeToRespond = float(ParamsParser.getParamsListBySection(experiment, taskType)["timetorespond"])
        self.validKeys = ParamsParser.getParamsListBySection(experiment, taskType)["validkbkeys"].replace(" ", "").split(
        ",")
        trialRTClock = core.Clock()
        trialRTClock.reset()

    def displayStimuli(self, window, stimulus, isDemo):

        window.flip()

        ScreenUtilities.addText(window,stimulus,pos=(0,0),color='white', height=200)
        # ScreenUtilities.addDemoText(window,  isDemo, pos= (0 , 500))
        trialRTClock = core.Clock()
        trialRTClock.reset()
        window.flip()
        res= ScreenUtilities.getKB(self.experiment, window, self.stimulusTime, self.validKeys,clock=trialRTClock,addNoResponseMsg=False)
        try:
            waitTime = float(res[0][1])
            ScreenUtilities.waitSeconds(self.stimulusTime - waitTime)
        except: pass
        return [res, trialRTClock]

    def displayFixation(self, window, isDemo):
        window.flip()
        # ScreenUtilities.addDemoText(window,  isDemo, pos= (0 , 500))
        ScreenUtilities.addText(window,"+")
        window.flip()
        ScreenUtilities.waitSeconds(self.fixationTime)
        window.flip()

    def getResponse(self, window, response, res, trialRTClock,isDemo):
        window.flip()
        try:
            respondedBefore = float(res[0][1])
            ScreenUtilities.waitSeconds(self.timeToRespond)

        except:
            res= ScreenUtilities.getKB(self.experiment, window, self.timeToRespond, self.validKeys,clock=trialRTClock,addNoResponseMsg=False)
            try:
                waitTime=float(res[0][1])
                ScreenUtilities.waitSeconds(self.timeToRespond-waitTime)
            except: pass
        return self.res2data(res, response)

    def res2data(self, result, response):

        kbData = {}

        if result == GeneralUtilities.ResponeTypes.TIMEOUT_NO_KEY_PRESSED:
            response.status = ResponeStatus.NO_USER_RESPONSE
            kbData['kbPressed'] = GeneralUtilities.ResponeTypes.TIMEOUT_NO_KEY_PRESSED[0]
            kbData['kbRT'] = "N/A"
            return kbData

        response.status = ResponeStatus.PRESSED
        kbData['kbPressed'] = result[0][0]
        kbData['kbRT'] = result[0][1]
        return kbData

    def responseCsvCols(self):
        return ["trialType", "outcome", "kbPressed" , "kbRT"]

class BanditExecutor(TaskExecutorAbstract):

    def __init__(self, experiment,  taskType):
        self.experiment = experiment
        self.timeToRespond = float(ParamsParser.getParamsListBySection(experiment, taskType)["timetorespond"])
        self.validKeys = ParamsParser.getParamsListBySection(experiment, taskType)["validkbkeys"].replace(" ","").split(",")
        self.slots= ParamsParser.getParamsListBySection(experiment, taskType)["slots"].replace(" ","").split(",")
        self.dir=ParamsParser.getParamValueByName(experiment,"basedir")+'/bandit/'
        self.picSize = map(int, ParamsParser.getParamValueByName(experiment, "picsize").split(","))
        #self.feedbackTime = float(ParamsParser.getParamsListBySection(experiment, taskType)["feedbacktime"])
        self.TTT = float(ParamsParser.getParamsListBySection(experiment, taskType)["totaltrialtime"])

    def displayStimuli(self, window, stimuli, trialType, response, isDemo):
        window.flip()
        images = []
        images += ScreenUtilities.addImages(stimuli[0], window, self.picSize)
        frames=ScreenUtilities.addFrameForImages(images, window, self.picSize)

        ScreenUtilities.addDemoText(window,  isDemo, pos= (0 , 500))
        trialRTClock = core.Clock()
        trialRTClock.reset()

        window.flip()
        res= ScreenUtilities.getKB(self.experiment, window, self.timeToRespond, self.validKeys,clock =trialRTClock, addNoResponseMsg = False, cross=True)
        drawFrameForSelectedImage(res,images,frames,self.validKeys)
        AnimateStim(res,images,self.validKeys,window)
        if isinstance(res[0][1], float):
            index=getKBIndex(self.validKeys,res[0][0])
            response.addToDict("indexChosen",index)
            win = 'You won \n %i points' %(stimuli[0][index].value)
            self.generateWinFeedback(window,index)
            ScreenUtilities.addText(window, win, height=50,pos=(-50,0), color='black')
            window.flip()
            ScreenUtilities.waitSeconds(0.9)
        else: response.addToDict("indexChosen","N/A")

        window.flip()

        return self.res2data(res, response, stimuli[0])

    def generateWinFeedback(self, window,index):
        chosenSlot = self.dir + self.slots[index] + '.JPG'
        curSlot = Stimuli.Stimuli(chosenSlot, 0, 0, index=9999)
        ScreenUtilities.addImage(window, curSlot,self.picSize)

    def res2data(self, result, response, stimuli):

        kbData = {}

        if result == GeneralUtilities.ResponeTypes.TIMEOUT_NO_KEY_PRESSED:
            response.status = ResponeStatus.NO_USER_RESPONSE
            kbData['kbPressed']  = GeneralUtilities.ResponeTypes.TIMEOUT_NO_KEY_PRESSED[0]
            kbData['kbRT'] = "N/A"
            kbData['Chosen'] = "N/A"
            kbData['reward'] = "N/A"
            return kbData

        response.status = ResponeStatus.PRESSED
        kbData['kbPressed']  = result[0][0]
        kbData['kbRT'] = result[0][1]
        index = getKBIndex(self.validKeys, result[0][0])
        kbData['Chosen'] = stimuli[index].fileName.split("/")[-2]

        kbData['reward']= stimuli[index].value
        return kbData

    def responseCsvCols(self):
        return ["Chosen", "indexChosen","reward", "reward_0", "reward_1", "reward_2", "kbRT", "kbPressed"  ]

    def displayFixation(self, window, res, isDemo):
        window.flip()
        ScreenUtilities.addText(window,"+")
        try: TimeLeft=self.TTT-res['kbRT']-2.8
        except: TimeLeft=self.TTT-self.timeToRespond-2.1
        window.flip()
        ScreenUtilities.waitSeconds(TimeLeft)

    def generateFeedback(self, window, kbPressed, stimuliTuple):
        responseCorrect = self.isResponseCorrect(kbPressed, stimuliTuple)
        if responseCorrect:
            self.__generatePositiveFeedback(window)
        else:
            self.__generateNegativeFeedback(window)

        window.flip()
        ScreenUtilities.waitSeconds(self.feedbackTime)

        return responseCorrect

    def isResponseCorrect(self, kbPressed, stimuliTuple):

        stimuliLocation = self.validKeys.split(",").index(kbPressed)
        chosenStimuli = stimuliTuple[stimuliLocation]

        return self.isStimuliReinforced(chosenStimuli)

    def isStimuliReinforced(self, stimuliTuple):
        return "brand" in stimuliTuple.fileName

    def __generatePositiveFeedback(self, window):

        positiveStimuli = Stimuli.Stimuli(self.positiveFeedbackFilename, 0, 0, index =  9999)
        feedbackStimuli = []
        feedbackStimuli.append(positiveStimuli)
        ScreenUtilities.addImages(feedbackStimuli, window , self.picSize)

    def __generateNegativeFeedback(self, window):

        negativeStimuli = Stimuli.Stimuli(self.negativeFeedbackFilename, xPos=0,yPos=0, index = 9999)
        feedbackStimuli = []
        feedbackStimuli.append(negativeStimuli)
        ScreenUtilities.addImages(feedbackStimuli, window , self.picSize)



class ValueExecutor(TaskExecutorAbstract):

    def __init__(self, experiment,  taskType):
        self.experiment = experiment
        self.picSize = map(int, ParamsParser.getParamValueByName(experiment, "picsize").split(","))
        self.timeToResponse = float(ParamsParser.getParamsListBySection(experiment, taskType)["timetoresponse"])
        self.keyList = ParamsParser.getParamsListBySection(experiment, taskType)["validkbkeys"]
        self.iti = int(ParamsParser.getParamsListBySection(experiment, taskType)["iti"])
        self.stimulusTime = float(ParamsParser.getParamsListBySection(experiment, taskType)["stimulustime"])
        self.rewardFile = self.negativeFeedbackFilename = experiment.baseDir + "/" + taskType + '/' + ParamsParser.getParamValueByName(experiment, "rewardFileName")  + '.png'

    def displayStimuli(self, window, stimuliTuple, response, isDemo, flip = True):

        images = ScreenUtilities.addImages(stimuliTuple, window, self.picSize)
        demoText = ScreenUtilities.addDemoText(window,  isDemo, pos= (0 , 300))
        [image.draw() for image in images]
        if flip:
            window.flip()
            ScreenUtilities.waitSeconds(self.stimulusTime)

        return images
    def displayReinforcment(self, window, response, text, images):

        gabor = ScreenUtilities.createGabor(window)
        gabor.draw()
        ScreenUtilities.addText(window, text, bold=False)
        trialRTClock = core.Clock()
        trialRTClock.reset()

        window.flip()
        ScreenUtilities.waitSeconds(0.000001)

        res = ScreenUtilities.getKB(self.experiment, self.experiment.window, self.timeToResponse, clock=trialRTClock,waitNoRespone=(text=="+0") ,addNoResponseMsg=(text=="+10"), keyList= self.keyList)

        if not isinstance(res[0][1], float): #no key pressed
            window.flip()
            return self.res2data(res , response)

        timeLeft = self.timeToResponse - res[0][1]
        if text == "+0":
            ScreenUtilities.waitSeconds(timeLeft)
            window.flip()
            ScreenUtilities.waitSeconds(self.iti)

        if text == "+10":
            [image.draw() for image in images]
            gabor.draw()
            ScreenUtilities.addText(self.experiment.window, text=text, color='yellow', bold=False)

            ScreenUtilities.waitSeconds(0.00001)
            window.flip()

            ScreenUtilities.waitSeconds(timeLeft)

            window.flip()
            ScreenUtilities.waitSeconds(self.iti)

        return self.res2data(res , response)

    def res2data(self, result, response):

        kbData = {}

        if result == GeneralUtilities.ResponeTypes.TIMEOUT_NO_KEY_PRESSED:
            response.status = ResponeStatus.NO_USER_RESPONSE
            kbData['kbPressed']  = GeneralUtilities.ResponeTypes.TIMEOUT_NO_KEY_PRESSED[0]
            kbData['kbRT'] = "N/A"
            return kbData

        response.status = ResponeStatus.PRESSED
        kbData['kbPressed']  = result[0][0]
        kbData['kbRT'] = result[0][1]
        return kbData

    def responseCsvCols(self):
        return ["kbPressed" , "kbRT"]


def applyWithinTripletsRandom(images):

    for index, st in enumerate(images):
        indexToSwitch = random.randint(0,len(images)-1)
        switchImagesPos(st,  images[indexToSwitch])

def switchImagesPos(st1, st2):
    if st1 == st2:
        return
    tempX = st1.xPos
    tempY = st1.yPos
    st1.xPos = st2.xPos
    st1.ypos = st2.yPos
    st2.xPos = tempX
    st2.yPos = tempY

def switchPicPos(pic1Pos,pic2Pos):
    tempX = pic1Pos[0]
    tempY = pic1Pos[1]
    pic1Pos[0] = pic2Pos[0]
    pic1Pos[1] = pic2Pos[1]
    pic2Pos[0] = tempX
    pic2Pos[1] = tempY
