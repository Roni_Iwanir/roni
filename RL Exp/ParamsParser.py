#!/usr/bin/env python

from ConfigParser import SafeConfigParser

def getParamValueByName(experiment, paramName):

    parser = SafeConfigParser()
    parser.read(experiment.getParamsFile())
    for sectionName in parser.sections():
        for name, value in parser.items(sectionName):
            if (name.lower() == paramName.lower()):
                return value

def getParamsListBySection(experiment, section):

    sectionDict = {}
    parser = SafeConfigParser()
    parser.read(experiment.getParamsFile())
    for sectionName in parser.sections():
        if sectionName == section:
            for name, value in parser.items(sectionName):
                sectionDict[name] = value

    return sectionDict
