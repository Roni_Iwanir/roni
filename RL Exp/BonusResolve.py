import ExtractDataFromOutput
import ScreenUtilities
import sys
import ScreenUtilities
import Experiment
import os
import shutil
PARAM_FILE = "paramsFile.ini"
from ConfigParser import SafeConfigParser

DropDir='/Users/ExpRoom2/Dropbox/experimentsOutput/RL/'

class Bonus:
    def __init__(self, id, baseDir):
        self.__id = id
        self.__paramsFile = PARAM_FILE
        self.__baseDir = baseDir

def getParamValueByName( paramName):

    parser = SafeConfigParser()
    parser.read(PARAM_FILE)
    for sectionName in parser.sections():
        for name, value in parser.items(sectionName):
            if (name.lower() == paramName.lower()):
                return value

def copy_file(src, dst):
    d = os.path.dirname(dst)
    if not os.path.exists(d):
        print 'Make directory: ', d
        os.makedirs(d)
    print 'copy', src, 'to', dst
    shutil.copy(src, dst)

#def copy_Dir(src,sdt):
    #(src_dir,dst_dir)

def main():
    subjectID = sys.argv[1]
    window=ScreenUtilities.defineInitialWindow(1920,1080)
    baseDir = getParamValueByName( "basedir")

    FileName = ExtractDataFromOutput.ExtractData(subjectID, baseDir, [4, 5])
    datafile = file(FileName, 'r')
    mylines = datafile.read()
    trial_lines = mylines.split("\n")

    bonus = 0
    for trial_line in trial_lines[:-1]:
        trial = trial_line.split(',')
        [choice, reward, subj] = map(int, trial)
        text = 'Congratulations! \n \nYou get a bonus of %.2f shekels' % round(bonus, 1)
        ScreenUtilities.addText(window, text)
        ScreenUtilities.flipWindow(window, 0.005)
        bonus += float(reward) / 1000

    text = 'Congratulations! \n \nYou get a bonus of %.2f shekels' % round(bonus, 1)
    ScreenUtilities.addText(window, text)
    window.flip()
    ScreenUtilities.waitSeconds(5)
    subDir=baseDir+"/outputs/"+subjectID
    OSPAN_file='/OSPAN/outputs/'+subjectID+'_OSPAN.csv'
    csvData= 'ForAnalysis/'+subjectID+'.csv'

    shutil.copytree(subDir, DropDir+"/outputs/"+subjectID)
    copy_file(baseDir+OSPAN_file,DropDir+OSPAN_file)
    copy_file(baseDir+"/outputs/"+csvData,DropDir+csvData)
main()

exit()