#
# BaseDir= 'C:/Users/Tali/Dropbox/experimentsOutput/roni/MachineLearning'
# desiredCols= [ 4, 5]
# subjectID='193'

Task= ["first", "second", "third"]
def ExtractData(subjectID, BaseDir, desiredCols):
    nth = {"first": 1, "second": 2, "third": 3, "fourth": 4}

    for TaskNum in Task:
        toWrite = ""
        filename = BaseDir + '/outputs/' + subjectID + '/' + 'LRE-subject#' + subjectID + '_output_bandit-' + TaskNum + '_bandit.csv'

        datafile = file(filename, 'r')
        mylines = datafile.read()
        NewFileName = BaseDir + '/outputs/ForAnalysis/' + subjectID + '.csv'
        f = open(NewFileName, 'a')

        trial_lines = mylines.split("\n")
        for trial_line in trial_lines[1:101]:
            trial = trial_line.split(',')

            try:
                toWrite += str(int(trial[desiredCols[0]])+1) + ","
            except: continue
            try:
                toWrite += str(int(float(trial[desiredCols[1]])))+ ","
            except: continue
            toWrite += str(nth[TaskNum])
            #for col in desiredCols:
            #    toWrite += str(trial[col])+","
            #toWrite=toWrite[:-1]
            toWrite += "\n"

        f.write(toWrite)
    f.close()
    return NewFileName



#ExtractData(subjectID, BaseDir, desiredCols)
