#!/usr/bin/env python

import ScreenUtilities
import ParamsParser
import TaskExecutor
import Exceptions
import Experiment

class SingleTrialStimuliGroup():

    def __init__(self,experiment, stimuliGroup, taskType, trialType):
        self.experiment = experiment
        self.stimuliGroup = stimuliGroup
        self.taskType = taskType
        self.trialType= trialType

    def present(self, window, response, isDemo, other):
        try:

            if self.taskType == Experiment.TaskTypes.NBACK:
                return TaskExecutor.nBackTaskExecutor(self.experiment, window, self.taskType[0], self.stimuliGroup[0], self.trialType, response, isDemo, other)

            if self.taskType == Experiment.TaskTypes.BANDIT:
                return TaskExecutor.banditTaskExecutor(self.experiment, window, self.taskType[0], self.stimuliGroup, self.trialType, response, isDemo, other)



        except Exceptions.KbNotPressedException as e:
            self.experiment.logger.error(e.msg)
            self.experiment.logger.info("trial skipped")
            print e.msg


    def __str__(self):
        s = ""
        for stimuli in self.stimuliGroup:
            s += str( stimuli)
        return s

    def __iter__(self):
        for x in self.stimuliGroup:
            yield x


class Stimuli():

    def __init__(self, name, xPos, yPos, index, value=""):
        self.fileName = name
        self.index = index
        self.xPos = xPos
        self.yPos = yPos
        self.value = value

    def __str__(self):
        return "stimulus num %d , stimulus name %s, (x,y) = (%d, %d)  " %(self.index, self.fileName, self.xPos, self.yPos)

    def getGroupName(self):
        try:
            return self.fileName.split("/")[-1].split("-")[0]
        except:
            return "Group name not specified"

    def getFileName(self, withGroup=False):

        if withGroup:
            return self.fileName
        else:
            try:
                return self.fileName.split("-")[1]
            except:
                return self.fileName

    def setPos(self, pos):
        self.xPos = pos[0]
        self.yPos = pos[1]